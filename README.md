![OS](https://badgen.net/badge/OS/Android?icon=https://raw.githubusercontent.com/androiddevnotes/awesome-android-kotlin-apps/master/assets/android.svg&color=3ddc84)

# Projet React Native



### Développeurs :

> Antoine ACQUART

> Mattéo LOPES



## Le Projet


**TIME BREAKER**

![icon](assets/icon.png)

### Contexte : 

Pour la mise en contexte, c'est une application qui rassemble plusieurs mini-jeux. Au lancement d'une partie, le joueur ce verra attribuer un mini jeu qu'il doit réussir le plus vite possible, ou le passer. D'autre mini-jeux seront ensuite proposées de façon aléatoire. Le joueur possède une minute pour réussir à récolter le plus de points en fonction de ses réussites. 



### Les pages :


<u>**Le menu :**</u>

Tous simplement la page d'accueil où se retrouve l'utilisateur qui lance l'application

Il peut naviguer entre les différentes pages, commencer à jouer et gérer son compte


<p align="center">
  <img src="menu.jpg" alt="menu" height="600" margin="1">
</p>


<u>**Les jeux :**</u>

La page de jeu est un défiler de mini-jeux, dans laquelle le joueur peut répondre où passer au mini-jeu suivant

Une fois le temps impartie, le jeu s'arrête et les résultats finaux sont afficher

<p align="center">
  <img src="jeu.jpg" alt="jeu" height="600" margin="1">
</p>


<u>**L'historique :**</u>

Affiche la liste des parties précédentes avec leurs résultats détaillés et le score afficher

Quand une partie est fini, le joueur est dirigé vers cette page

<p align="center">
  <img src="historique.jpg" alt="historique" height="600" margin="1">
</p>



<u>**Les règles :**</u>

Une page explicatif du déroulement et des explications dynamique avec la liste des mini-jeux 

<p align="center">
  <img src="regles.jpg" alt="règles" height="600" margin="1">
</p>


<u>**Le classement :**</u>

Le joueur peut envoyer son score sur le classement général du jeu ou sur des classements spécifique grâce au tag personnalisé. 

Il peut notamment visualiser les tableaux des scores

<p align="center">
  <img src="classement.jpg" alt="classement" height="600" margin="1">
</p>



## Lancer l'application



Cloner le projet, puis exécutez les commandes 

```bash
npm i
expo start 
# en mode web
w
# ou en mode android
a
```

**ou**

Installer l'application en .apk sur [ce lien](https://reactnativefinalproject-6f28c.web.app)


## Module utilisés 

[react-native](https://www.npmjs.com/package/react-native)

[@react-navigation/native](https://www.npmjs.com/package/react-navigation)

[@expo/vector-icons](https://www.npmjs.com/package/@expo/vector-icons)

[@expo-constants](https://www.npmjs.com/package/expo-constants)

[@firebase/auth](https://www.npmjs.com/package/@firebase/auth)

[@firebase/firestore](https://www.npmjs.com/package/@firebase/firestore)

[@react-native-async-storage/async-storage](https://www.npmjs.com/package/@react-native-async-storage/async-storage)

[rxjs](https://www.npmjs.com/package/rxjs)

## Composants natifs

- [Share](https://reactnative.dev/docs/share)

- [Vibration](https://reactnative.dev/docs/vibration)

- [Storage](https://react-native-async-storage.github.io/async-storage/docs/usage/)

## API

- Firebase : Authentification
- FireStore : Stockage du classement
