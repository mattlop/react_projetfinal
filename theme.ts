export enum ThemeColor {
    PRIMARY = '#36393F',
    PRIMARY_SHADE = '#26292F',
    PRIMARY_THIN = '#46494F',
    PRIMARY_LIGHT = '#969A9F',
    PRIMARY_TEXT = '#F0F0F0',
    SECONDARY_TEXT = '#C0C0C0',
    TERTIARY_TEXT = '#A0A0A0',
    SUCCESS = '#75C54A',
    WARNING = '#FF9671',
    DANGER = '#F04343',
    WHITE = '#F0F0F0',
    BLACK = '#100c08',
    YELLOW = '#F9F871',
    BLUE = '#0081CF',
}

export const timeColors = [
    "#8A1616", // ROUGE
    "#DCA11D", // ORANGE
    "#FFD24A", // JAUNE
    "#258E37", // JAUNE - VERT
    "#3CB597", // VERT
    "#00D6D5", // TURQUOISE
    "#006EC4", // BLEU
    "#5C51AD", // VIOLET
    "#B10065", // POURPRE
    "#FF8AC3", // ROSE
];