import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.reactfinalapp.app',
  appName: 'aa_lm_reactnatif_projetfinal',
  webDir: 'html',
  bundledWebRuntime: false
};

export default config;
