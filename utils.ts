import { CommonActions } from "@react-navigation/native";
import { timeColors } from "./theme";

export const generateColorByTimestamp = (timestamp: number): string => {
    const minute = new Date(timestamp).getMinutes();
    return timeColors[minute % 10];
}

export const getDisplayableTime = (millis: number): string => {
    return ((millis % 60000) / 1000).toFixed(2);
};

export const insertBeforeLast = (routeName: string, params?: any) => (state: any) => {
    const routes = [
        ...state.routes.slice(0, -1),
        { name: routeName, params },
        state.routes[state.routes.length - 1],
    ];

    return CommonActions.reset({
        ...state,
        routes,
        index: routes.length - 1,
    });
};
