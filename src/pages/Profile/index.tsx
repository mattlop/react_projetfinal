import { useIsFocused } from '@react-navigation/native';
import * as React from 'react'
import { useEffect, useRef, useState } from 'react';
import { Keyboard, View, Platform, Text, StyleSheet, ActivityIndicator, Pressable, EmitterSubscription } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';
import { ThemeColor } from '../../../theme';
import { ERROR_CONNEXION } from '../../../types/types';
import { useFireBaseAuth } from '../../hooks';
import { User } from "firebase/auth";
import i18n, { Lang } from '../../../locale';

const Profile = ({ navigation }: any) => {
    const { user, isConnected, signOutUser, signIn, register, signWithGoogle } = useFireBaseAuth();
    const [email, setEmail] = useState<string>("")
    const [passwd, setPasswd] = useState<string>("")
    const [confirmPasswd, setConfirmPasswd] = useState<string>("")
    const [loading, setLoading] = useState<boolean>(false)
    const [containerButtons, setContainerButtons] = useState<string>("flex")
    const [showRegister, setShowRegister] = useState<boolean>(false)
    const [errorMessage, setErrorMessage] = useState<string | null>(null)
    const [errorMessageColor, setErrorMessageColor] = useState<string>(ThemeColor.TERTIARY_TEXT)

    const [keyboardDidShowListener, setShowListener] = useState<EmitterSubscription | undefined>();
    const [keyboardDidHideListener, setHideListener] = useState<EmitterSubscription | undefined>();

    const TI_email = useRef<TextInput>(null);
    const TI_passwd = useRef<TextInput>(null);
    const TI_passwdconfirm = useRef<TextInput>(null);

    if (isConnected == null) {
        <View style={styles.mainContainer}>
            <ActivityIndicator size={"large"} color={"white"} />
        </View>
    }

    const tryingSign = () => {
        if (email == "") {
            setErrorMessage("Aucun email inscrit")
            setErrorMessageColor(ThemeColor.DANGER)
            return;
        }
        if (passwd == "") {
            setErrorMessage("Aucun mot de passe inséré")
            setErrorMessageColor(ThemeColor.DANGER)
            return;
        }
        if (passwd.length < 8) {
            setErrorMessage("Mot de passe trop court")
            setErrorMessageColor(ThemeColor.DANGER)
            return;
        }
        if (showRegister) {
            tryingRegister()
        } else {
            tryingSignIn()
        }
    }

    const tryingSignIn = () => {
        setLoading(true)
        signIn(email, passwd).then(() => {
            setLoading(false)
        }).catch((reason) => {
            setLoading(false)
            if (reason == ERROR_CONNEXION.MAIL_NOT_EXIST) {
                setShowRegister(true)
                setErrorMessage(i18n.t(Lang.NOT_REGISTER_CONFIRM_PASSWORD))
                setErrorMessageColor(ThemeColor.WARNING)
            } else if (reason == ERROR_CONNEXION.MAIL_NOT_VALIDE) {
                setErrorMessage(i18n.t(Lang.MAIL_NOT_VALIDE))
                setErrorMessageColor(ThemeColor.DANGER)
            } else if (reason == ERROR_CONNEXION.BAD_CREDENTIAL) {
                setErrorMessage(i18n.t(Lang.BAD_CREDENTIAL))
                setErrorMessageColor(ThemeColor.DANGER)
            } else if (reason == ERROR_CONNEXION.UNKNOWN) {
                setErrorMessage(i18n.t(Lang.UNKNOWN_ERROR))
                setErrorMessageColor(ThemeColor.DANGER)
            }
        })
    }

    const tryingRegister = () => {
        if (passwd == confirmPasswd) {
            setErrorMessage(i18n.t(Lang.BAD_PASSWORD_CONFIRMATION))
            setErrorMessageColor(ThemeColor.DANGER)
            return
        }
        setLoading(true)
        register(email, passwd).then(() => {
            setLoading(false)
        }).catch((reason) => {
            setLoading(false)
            if (reason == ERROR_CONNEXION.ALREADY_EXIST) {
                setErrorMessage(i18n.t(Lang.ACCOUNT_ALREADY_EXIST))
                setErrorMessageColor(ThemeColor.WARNING)
            } else if (reason == ERROR_CONNEXION.UNKNOWN) {
                setErrorMessage(i18n.t(Lang.UNKNOWN_ERROR))
                setErrorMessageColor(ThemeColor.DANGER)
            }
        })
    }

    const isFocused = useIsFocused();

    useEffect(() => {
        if (isFocused) {
            if (keyboardDidShowListener) {
                keyboardDidShowListener?.remove();
                keyboardDidHideListener?.remove();
            }
            setShowListener(Keyboard.addListener('keyboardDidShow', _keyboardDidShow))
            setHideListener(Keyboard.addListener('keyboardDidHide', _keyboardDidHide))
        } else {
            if (keyboardDidShowListener) {
                keyboardDidShowListener?.remove();
                keyboardDidHideListener?.remove();
            }
        }

        return (
            keyboardDidShowListener?.remove(),
            keyboardDidHideListener?.remove()
        )
    }, [isFocused])

    const _keyboardDidShow = () => {
        setContainerButtons("none")
    }

    const _keyboardDidHide = () => {
        setContainerButtons("flex")
    }

    useEffect(() => {
        if (user) {
            //console.log(user)
        }
    }, [user])

    return (
        <View style={styles.mainContainer}>
            {isConnected == true && (
                <View style={styles.mainContainer}>
                    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                        <Text style={{ ...textHeader, textAlign: "center" }}> Mail : {user?.email}</Text>
                        <Text style={{ ...textHeader, textAlign: "center" }}> Nom : {user?.displayName ? user?.displayName : `${user?.email?.split('.')[0]}`}</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                        <Pressable onPress={() => { signOutUser() }} style={styles.pressableConnect}>
                            <Text style={{ ...textRegular, fontSize: 20, textAlign: "center" }}>{i18n.t(Lang.LOGOUT)}</Text>
                        </Pressable>
                    </View>
                </View>
            )}
            {isConnected == false && (
                <View style={styles.mainContainer}>
                    <View style={styles.containerHeader}>
                        <View style={{ ...styles.containerHeaderElement, flex: 2 }}>
                            {!showRegister && (
                                <Text style={{ ...textHeader }}>{i18n.t(Lang.LOGIN)}</Text>
                            )}
                            {showRegister && (
                                <Text style={textHeader}>{i18n.t(Lang.REGISTER)}</Text>
                            )}
                        </View>
                        <View style={styles.containerHeaderElement}>
                            {errorMessage == null && (
                                <Text style={{ ...textLight, textAlign: "center" }}>{i18n.t(Lang.FILL_FORM_TO_REGISTER)}</Text>
                            )}
                            {errorMessage != null && (
                                <Text style={{ ...textLight, color: errorMessageColor, textAlign: "center" }}> {errorMessage} </Text>
                            )}
                        </View>
                    </View>
                    <View style={styles.containerInput}>
                        <View style={styles.containerInputElement}>
                            <Text style={{ ...textRegular, color: ThemeColor.WHITE, textAlign: 'center', fontSize: 20 }}>
                                {i18n.t(Lang.MAIL)}
                            </Text>
                            <TextInput ref={TI_email} onSubmitEditing={() => { TI_passwd?.current?.focus() }} value={email} onChangeText={setEmail} style={styles.inputElement} />
                        </View>
                        <View style={styles.containerInputElement}>
                            <Text style={{ ...textRegular, color: ThemeColor.WHITE, textAlign: 'center', fontSize: 20 }}>
                                {i18n.t(Lang.PASSWORD)}
                            </Text>
                            <TextInput secureTextEntry={true} ref={TI_passwd} onSubmitEditing={() => { if (showRegister) { TI_passwdconfirm?.current?.focus() } else { tryingSign() } }} value={passwd} onChangeText={setPasswd} style={styles.inputElement} />
                        </View>
                        {showRegister && (
                            <View style={styles.containerInputElement}>
                                <Text style={{ ...textRegular, color: ThemeColor.WHITE, textAlign: 'center', fontSize: 20 }}>
                                    {i18n.t(Lang.CONFIRM_PASSWORD)}
                                </Text>
                                <TextInput secureTextEntry={true} ref={TI_passwdconfirm} onSubmitEditing={() => { tryingSign() }} value={confirmPasswd} onChangeText={setConfirmPasswd} style={styles.inputElement} />
                            </View>
                        )}
                    </View>
                    <View style={{ ...containerConnect, display: containerButtons, flex: containerButtons == "flex" ? 2 : 0 }}>
                        <Pressable disabled={loading} style={styles.pressableConnect} onPress={() => { tryingSign() }}>
                            {!showRegister && (
                                <Text style={{ ...textRegular, fontSize: 20, textAlign: "center" }}> {i18n.t(Lang.LOGIN)} </Text>
                            )}
                            {showRegister && (
                                <Text style={{ ...textRegular, fontSize: 20, textAlign: "center" }}> {i18n.t(Lang.REGISTER)} </Text>
                            )}
                        </Pressable>

                        {/* <Pressable onPress={() => { signWithGoogle() }} disabled={loading} style={styles.pressableConnect}>
                            <Text style={{ ...textRegular, fontSize: 20, textAlign: "center" }}> Connexion Google </Text>
                        </Pressable> */}
                    </View>
                </View>
            )}

            {loading == true && (
                <View style={styles.containerLoading}>
                    <ActivityIndicator size={"large"} color={"white"} />
                </View>
            )}
        </View>
    )
}

const textLight = {
    fontSize: 12,
    //fontFamily: 'RobotoSlab_300Light',
    marginBottom: 10,
    color: ThemeColor.TERTIARY_TEXT,
    flexWrap: "wrap"
}

const textRegular = {
    marginHorizontal: 5,
    fontSize: 14,
    //fontFamily: 'RobotoSlab_400Regular',
    marginBottom: 10,
    color: ThemeColor.SECONDARY_TEXT
}

const textHeader = {
    paddingVertical: 10,
    textAlign: 'center',
    fontSize: 23,
    //fontFamily: 'RobotoSlab_700Bold',
    color: ThemeColor.PRIMARY_TEXT
}

const containerConnect = {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: ThemeColor.PRIMARY,
    },
    containerLoading: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        backgroundColor: "#36393F55",
        justifyContent: "center",
        alignItems: "center"
    },
    containerHeader: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: 'column',
    },
    containerHeaderElement: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 20
    },
    containerInput: {
        flex: 3,
        alignItems: "center",
        justifyContent: "space-evenly",
        flexDirection: "column",
    },
    containerInputElement: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
    },
    inputElement: {
        height: 50,
        minWidth: 200,
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 16,
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 10,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
        textAlign: 'center',
    },
    pressableConnect: {
        marginBottom: 30,
        backgroundColor: ThemeColor.BLACK,
        alignContent: 'center',
        justifyContent: 'center',
        paddingTop: 8,
        width: 200,
        borderRadius: 5,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,

        elevation: 12,
    },
});

export default Profile