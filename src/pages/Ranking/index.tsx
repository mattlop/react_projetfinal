import React, { useEffect, useRef, useState } from 'react'
import { ActivityIndicator, Keyboard, Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'
import { ThemeColor } from '../../../theme'
import { useFireBaseAuth, useFireStore, useStorage } from '../../hooks'
import { Ionicons, AntDesign, FontAwesome, MaterialIcons, Entypo } from '@expo/vector-icons'
import { TextInput } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import { GameData, Scoreboard } from '../../../types/types'
import i18n, { Lang } from '../../../locale'

const Ranking = () => {
    const { getScoreboard, setOwnScoreInScoreboard, ready, user } = useFireStore()
    const { gameDatas, gameDatasLoaded } = useStorage()
    const [textSearch, setTextSearch] = useState<string>("")
    const [scoreboardLoaded, setScoreboardLoaded] = useState<Scoreboard | null>(null)
    const [scoreboardIsLoading, setScoreboardIsLoading] = useState<boolean>(false)

    useEffect(() => {
        startSearchHash(textSearch)
    }, [ready])

    const startSearchHash = (tag: string) => {
        if (tag == '' || tag == null) tag = 'general'
        setScoreboardIsLoading(true)
        getScoreboard(tag.toLocaleLowerCase()).then((scoreboard: Scoreboard) => {
            setScoreboardLoaded(scoreboard)
            setScoreboardIsLoading(false)
        }).catch((reason: string) => {
            if (reason == "scoreboard not exist") {
                setScoreboardLoaded({ players: [], tag: tag })
            }
            setScoreboardIsLoading(false)
        })
    }

    const [sendScoreLoading, setSendScoreLoading] = useState<boolean>(false)
    const [showModalSendScore, setShowModalSendScore] = useState<boolean>(false)
    const [modalText, setModalText] = useState<string>("")
    const [modalColor, setModalColor] = useState<string>(ThemeColor.PRIMARY)

    const sendPlayerScore = (score: number, tag: string) => {
        setShowModalSendScore(true)
        setSendScoreLoading(true)
        if (tag == '' || tag == null) tag = 'general'
        setOwnScoreInScoreboard(score, tag).then(() => {
            setModalColor(ThemeColor.SUCCESS)
            setModalText(i18n.t(Lang.SEND))
        }).catch((reason: string) => {
            if (reason == "user already added") {
                setModalColor(ThemeColor.WARNING)
                setModalText(i18n.t(Lang.SCORE_ALREADY_SEND))
            } else {
                setModalColor(ThemeColor.DANGER)
                setModalText(i18n.t(Lang.ERROR_RETRY))
            }
        }).finally(() => { setSendScoreLoading(false) })
    }

    const getBestGames = () => {
        const bestGames: GameData[] = []
        gameDatas.forEach(gameData => {
            const similarBestGameIndex = bestGames.findIndex(bestGame => bestGame.mode.type == gameData.mode.type && bestGame.tag == gameData.tag)
            if (similarBestGameIndex == -1) bestGames.push(gameData)
            else if (bestGames[similarBestGameIndex]?.score < gameData.score) bestGames[similarBestGameIndex] = gameData
        })
        return bestGames
    }

    return (
        <View style={styles.mainContainer}>
            {showModalSendScore && (
                <Pressable style={styles.modalContainer} onPress={() => { if (!sendScoreLoading) { setShowModalSendScore(false) } }}>
                    {sendScoreLoading && (
                        <View style={styles.modalContent}>
                            <ActivityIndicator size={"small"} color={ThemeColor.WHITE} />
                            <Text style={{ ...textElement, color: ThemeColor.TERTIARY_TEXT, marginLeft: 10 }}>
                                {i18n.t(Lang.SENDING)}
                            </Text>
                        </View>
                    )}
                    {!sendScoreLoading && (
                        <View style={styles.modalContent}>
                            {modalColor == ThemeColor.SUCCESS && (
                                <FontAwesome name="check" size={24} color={modalColor} />
                            )}
                            {modalColor == ThemeColor.WARNING && (
                                <MaterialIcons name="error" size={24} color={modalColor} />
                            )}
                            {modalColor == ThemeColor.DANGER && (
                                <Entypo name="circle-with-cross" size={24} color={modalColor} />
                            )}
                            <Text style={{ ...textElement, color: modalColor, marginLeft: 10 }}>
                                {modalText}
                            </Text>
                        </View>
                    )}
                </Pressable>
            )}

            {ready == null && (
                <View style={styles.mainContainerCenter}>
                    <ActivityIndicator size={"large"} color={ThemeColor.WHITE} />
                </View>
            )}
            {ready == false && (
                <View style={styles.mainContainerCenter}>
                    <Ionicons style={{ marginRight: 10 }} name="warning-outline" size={24} color={ThemeColor.DANGER} />
                    <Text style={{ ...textElement, color: ThemeColor.DANGER }}>
                        {i18n.t(Lang.NOT_CONNECTED)}
                    </Text>
                </View>
            )}
            {ready == true && !gameDatasLoaded && (
                <View style={styles.mainContainerCenter}>
                    <ActivityIndicator size={"large"} color={ThemeColor.WHITE} />
                </View>
            )}

            {ready == true && gameDatasLoaded && (
                <View style={{ flex: 1 }}>
                    <View style={styles.searchContainer}>
                        <View style={styles.searchBar}>
                            <AntDesign name="search1" size={24} color="white" onPress={() => startSearchHash(textSearch)} />
                            <TextInput style={styles.searchText} onSubmitEditing={() => { startSearchHash(textSearch) }} onChangeText={(text) => {
                                setTextSearch(text)
                            }} value={textSearch} placeholder={i18n.t(Lang.SEARCH_TAG)}></TextInput>
                        </View>
                    </View>
                    {(!scoreboardIsLoading && scoreboardLoaded) && (
                        <View style={styles.rankingContainer}>
                            <SafeAreaView style={styles.rankingScrollContainer}>
                                <View style={styles.rankingHeader}>
                                    <View style={styles.rankingRankView}>
                                        <Text style={{ ...textElement, fontSize: 16, fontWeight: 'bold' }}>{i18n.t(Lang.RANK)}</Text>
                                    </View>
                                    <View style={styles.rankingUsernameView}>
                                        <Text style={{ ...textElement, fontSize: 16, fontWeight: 'bold' }}>{i18n.t(Lang.NAME)}</Text>
                                    </View>
                                    <View style={styles.rankingScoreView}>
                                        <Text style={{ ...textElement, fontSize: 16, fontWeight: 'bold' }}>{i18n.t(Lang.SCORE)}</Text>
                                    </View>
                                </View>
                                <ScrollView showsVerticalScrollIndicator={false}>
                                    {scoreboardLoaded.players.map((player, index) => (
                                        <View key={index} style={styles.rankingElement}>
                                            <View style={styles.rankingRankView}>
                                                <Text style={{ ...textElement, fontSize: 13, color: ThemeColor.SECONDARY_TEXT }}>{index + 1}</Text>
                                            </View>
                                            <View style={styles.rankingUsernameView}>
                                                <Text style={{ ...textElement, fontSize: 13, color: ThemeColor.SECONDARY_TEXT }}>{player.username ? player.username : "Guest"}</Text>
                                            </View>
                                            <View style={styles.rankingScoreView}>
                                                <Text style={{ ...textElement, fontSize: 13, color: ThemeColor.SECONDARY_TEXT }}>{player.score}</Text>
                                            </View>
                                        </View>
                                    ))}
                                </ScrollView>
                            </SafeAreaView>
                        </View>
                    )}
                    {scoreboardIsLoading && (
                        <View style={styles.loadingContainer}>
                            <ActivityIndicator size={"large"} color={ThemeColor.WHITE} />
                        </View>
                    )}

                    <View style={{ ...historyContainer, flex: (!scoreboardLoaded && !scoreboardIsLoading ? 5 : 4) }}>
                        <SafeAreaView style={styles.historyScrollContainer}>
                            <View style={styles.rankingHeader}>
                                <Text style={{ ...textElement, fontWeight: 'bold' }}>{i18n.t(Lang.BEST_PARTY)}</Text>
                            </View>
                            <View style={styles.historyColumnsHeader}>
                                <View style={styles.historyScoreColumn}>
                                    <Text style={styles.historyColumnText} numberOfLines={1}>{i18n.t(Lang.SCORE)}</Text>
                                </View>
                                <View style={styles.historyModeColumn}>
                                    <Text style={styles.historyColumnText} numberOfLines={1}>{i18n.t(Lang.MODE)}</Text>
                                </View>
                                <View style={styles.historyTagColumn}>
                                    <Text style={styles.historyColumnText} numberOfLines={1}>{i18n.t(Lang.TAG)}</Text>
                                </View>
                                <View style={styles.historyShareColumn}>
                                    <Text style={styles.historyColumnText} numberOfLines={1}>{i18n.t(Lang.SHARE)}</Text>
                                </View>
                            </View>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {getBestGames().map((data, index) => (
                                    <View key={index} style={styles.historyElement}>
                                        <View style={styles.historyScoreView}>
                                            <Text style={styles.historyViewText} numberOfLines={1} adjustsFontSizeToFit>{data.score}</Text>
                                        </View>
                                        <View style={styles.historyModeView}>
                                            <Text style={styles.historyViewText} numberOfLines={1} adjustsFontSizeToFit>{data?.mode?.name}</Text>
                                        </View>
                                        <View style={styles.historyTagView}>
                                            <Text style={styles.historyViewText} numberOfLines={1} adjustsFontSizeToFit>{data.tag}</Text>
                                        </View>
                                        <View style={styles.historyShareView}>
                                            <Pressable style={styles.historyScoreboardSend} onPress={() => { sendPlayerScore(data.score, data.tag) }}>
                                                <FontAwesome name="send" size={12} color="white" />
                                            </Pressable>
                                        </View>
                                    </View>
                                ))}
                            </ScrollView>
                        </SafeAreaView>
                    </View>
                </View>
            )}
        </View>
    )
}

const textElement = {
    fontSize: 20,
    color: ThemeColor.PRIMARY_TEXT,
}

const historyContainer = {
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    margin: 10,
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: ThemeColor.PRIMARY,
        flex: 1,
    },
    mainContainerCenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    searchContainer: {
        //display: 'none',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchBar: {
        flex: 1,
        height: 40,
        marginHorizontal: 10,
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 100,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 12
    },
    searchText: {
        //fontFamily: 'RobotoSlab_400Regular',
        flex: 1,
        fontSize: 14,
        marginLeft: 10,
        color: ThemeColor.WHITE,
    },
    rankingContainer: {
        flex: 4,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        margin: 10,
    },
    rankingScrollContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        borderRadius: 10,
        backgroundColor: ThemeColor.PRIMARY_THIN
    },
    rankingHeader: {
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    rankingElement: {
        flexDirection: 'row',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: ThemeColor.PRIMARY,
    },
    rankingRankView: {
        flex: 1,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    rankingUsernameView: {
        flex: 2,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    rankingScoreView: {
        flex: 2,
        alignItems: 'center',
    },
    historyScrollContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        borderRadius: 10,
        backgroundColor: ThemeColor.PRIMARY_THIN
    },
    historyHeader: {
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    historyColumnsHeader: {
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        borderTopColor: ThemeColor.PRIMARY,
        borderTopWidth: 1
    },
    historyModeColumn: {
        flex: 2,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    historyScoreColumn: {
        flex: 1,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    historyTagColumn: {
        flex: 1,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    historyShareColumn: {
        flex: 1,
        alignItems: 'center',
    },
    historyColumnText: {
        color: ThemeColor.PRIMARY_LIGHT,
        fontSize: 14,
        textTransform: 'uppercase'
        //fontWeight: 'bold'
    },
    historyElement: {
        flexDirection: 'row',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: ThemeColor.PRIMARY,
    },
    historyViewText: {
        color: ThemeColor.SECONDARY_TEXT,
        fontSize: 13,
        textTransform: 'uppercase',
        fontWeight: 'bold'
        //fontWeight: 'bold'
    },
    historyModeView: {
        flex: 2,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    historyTagView: {
        flex: 1,
        alignItems: 'center',
    },
    historyScoreView: {
        flex: 1,
        alignItems: 'center',
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    historyShareView: {
        flex: 1,
        alignItems: 'center',
    },
    historyScoreboardShow: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        paddingVertical: 15,
        paddingHorizontal: 5,
        backgroundColor: ThemeColor.BLUE
    },
    historyScoreboardSend: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        paddingVertical: 5,
        paddingHorizontal: 15,
        backgroundColor: ThemeColor.BLUE
    },
    loadingContainer: {
        flex: 4,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
        borderRadius: 20,
        backgroundColor: ThemeColor.PRIMARY_THIN
    },
    modalContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: "#36393F99",
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 10,
    },
    modalContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default Ranking