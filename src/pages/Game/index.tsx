import { Pressable, StyleSheet, Text, Vibration, View } from 'react-native'
import { GeometryFinder, TextInputLevel, TimerBar } from '../../components'
import { useGame } from '../../hooks'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import { useEffect, useState } from 'react'
import { ThemeColor } from '../../../theme'
import React from 'react'
import { GeometryFinderLevelData, LevelData, TextInputLevelData } from '../../../types'

const gameDuration = 60
let countdownTimer: NodeJS.Timer | undefined
const clearTimer = () => {
    if (countdownTimer) {
        clearInterval(countdownTimer)
        countdownTimer = undefined
    }
}

const Game = ({ navigation, route }: any) => {
    const { currentDate, game, isRunning, isResultsDisplayed, setUserInput, startGame, startNextLevel, stopGame } = useGame()
    const [currentLevel, setCurrentLevel] = useState<LevelData | undefined>(game?.levels[game.levels.length - 1])
    const [countdown, setCountdown] = useState<number>(-1)
    const { gameModeIndex, tag, training } = route.params

    useEffect(() => {
        startGame(gameModeIndex, tag, training)
    }, [currentDate])

    useEffect(() => {
        return clearTimer
    }, [])

    useEffect(() => {
        setCurrentLevel(game?.levels[game.levels.length - 1])
        if (game && !countdownTimer) {
            if (game?.training) setCountdown(-1)
            else {
                setCountdown(gameDuration)
                let count = 0
                countdownTimer = setInterval(() => {
                    if (gameDuration - count <= 6) {
                        Vibration.vibrate([0, 100])
                    }
                    count++
                    setCountdown(gameDuration - count)
                }, 1000)
            }
        }
    }, [game])

    useEffect(() => {
        if (isResultsDisplayed) {
            clearTimer()
            navigation.reset({
                index: 0,
                routes: [{ name: 'History', params: { insertHomeInNav: true }, }],
            });
        }
    }, [isResultsDisplayed])

    return (
        <View style={styles.container}>
            {(isRunning && currentLevel) &&
                <View style={styles.container2}>
                    <View style={styles.timerContainer}>
                        <View style={styles.timer}>
                            <MaterialCommunityIcons style={{ alignSelf: 'center' }} name="timer-sand" size={40} color={ThemeColor.BLUE} />
                            {!game?.training &&
                                <Text style={styles.timerText}>{countdown}</Text>
                            }
                            {game?.training &&
                                <Text style={styles.timerText}>&#8734;</Text>
                            }
                        </View>
                        {countdown >= 0 &&
                            <TimerBar timerElapsed={() => stopGame()} seconds={gameDuration} autoRestart={false} />
                        }
                    </View>

                    <View style={styles.levelContainer}>
                        <View style={styles.levelHeader}>
                            <Text style={styles.levelIndex} adjustsFontSizeToFit>{game?.levels.length}</Text>
                            <Text style={styles.levelType} adjustsFontSizeToFit numberOfLines={1}>{currentLevel.levelType}</Text>
                        </View>

                        <View style={styles.levelContent}>
                            {currentLevel instanceof TextInputLevelData &&
                                <TextInputLevel level={currentLevel} onChangeText={(text: string) => setUserInput(text)} onSubmit={() => startNextLevel()} />
                            }
                            {currentLevel instanceof GeometryFinderLevelData &&
                                <GeometryFinder level={currentLevel} onChangeChoice={(text: string) => setUserInput(text)} onSubmit={() => startNextLevel()} />
                            }
                        </View>
                    </View>
                    <View style={styles.bottomContainer}>
                        <Pressable style={styles.skipButton} onPress={() => { startNextLevel() }} >
                            <Ionicons name='chevron-forward' size={40} color={ThemeColor.PRIMARY_TEXT} />
                        </Pressable>
                    </View>
                </View>
            }
        </View>)
}

const styles = StyleSheet.create({
    container: {
        //marginTop: Constants.statusBarHeight,
        flex: 1,
        //width: Dimensions.get('window').width,
        overflow: 'hidden',
        backgroundColor: ThemeColor.PRIMARY,
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    container2: {
        flex: 1,
        backgroundColor: ThemeColor.PRIMARY,
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    titleText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 30,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    timerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20,
    },
    timer: {
        //width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        padding: 5
    },
    timerText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 40,
        paddingVertical: 5,
        paddingHorizontal: 10,
        overflow: 'hidden',
    },
    levelContainer: {
        flex: 1,
        width: '90%',
        margin: 10,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: ThemeColor.PRIMARY,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    levelHeader: {
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderTopEndRadius: 20,
        borderBottomColor: ThemeColor.PRIMARY,
        borderBottomWidth: 1,
        overflow: 'hidden',
    },
    levelIndex: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 30,
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    levelType: {
        flex: 1,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 30,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    levelContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch'
    },
    bottomContainer: {
        paddingTop: 20,
        paddingBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    skipButton: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 200,
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderRadius: 50,
        backgroundColor: ThemeColor.BLUE,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
})

export default Game