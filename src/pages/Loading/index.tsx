import React from 'react'
import { Pressable, StyleSheet, Text, View } from 'react-native'
import i18n, { Lang } from '../../../locale'
import { ThemeColor } from '../../../theme'

const Loading = () => {
    return (
        <View style={styles.container}>
            <Text> {i18n.t(Lang.LOADING)} ... </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY
    }
})

export default Loading