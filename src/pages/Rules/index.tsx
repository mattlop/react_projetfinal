import React, { useEffect, useState } from 'react'
import { Pressable, ScrollView, Vibration, StyleSheet, Text, View } from 'react-native'
import i18n, { Lang } from '../../../locale'
import { ThemeColor } from '../../../theme'
import { Hash } from '../../../types'
import { Ionicons } from '@expo/vector-icons'
import { GeometryFinder, TextInputLevel, TimerBar } from '../../components'
import GeometryFinderLevelData from '../../../types/GeometryFinderLevelData'

const Rules = ({ navigation }: any) => {

    const [scrollView, setScrollView] = useState<any>()

    const setRefScrollView = (element: any) => {
        setScrollView(element)
    }

    useEffect(() => {
        if (scrollView) {
            scrollView.scrollTo({ x: 0, animated: false }) // Initialise le scrollView en haut de la page
        }
    }, [scrollView])

    return (
        <View style={styles.mainContainer}>
            <ScrollView ref={setRefScrollView} style={{ flex: 4 }}>
                <View style={styles.scrollContainer}>
                    {/* <Text style={{ ...textHeader, color: ThemeColor.BLUE }}> {i18n.t(Lang.RULES_MAIN_TITLE)} </Text>

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_1)}
                    </Text> */}

                    <Text style={{ ...textHeader, color: ThemeColor.PRIMARY_TEXT }}> {i18n.t(Lang.RULES_TITLE_1)}  </Text>

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_2)}
                        <Text style={{ fontWeight: 'bold' }}>{i18n.t(Lang.RULES_TEXT_2_BOLD)} </Text>
                        {i18n.t(Lang.RULES_TEXT_2_NEXT)}
                    </Text>

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_3)}
                    </Text>

                    <Text style={{ ...textHeader, color: ThemeColor.PRIMARY_TEXT }}>{i18n.t(Lang.RULES_TITLE_2)}</Text>

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_4)}
                    </Text>
                    <View style={{ marginBottom: 10, alignItems: 'center' }}>
                        <TimerBar autoRestart={true} timerElapsed={() => { }} seconds={30} />
                    </View>

                    {/* <Text style={{ ...textLight, textAlign: 'center' }}> {i18n.t(Lang.RULES_SMALL_TEXT_1)} </Text> */}

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_5)}
                    </Text>
                    <Pressable style={styles.skipButton} onPress={() => { }} >
                        <Ionicons name='chevron-forward' size={30} color={ThemeColor.PRIMARY_TEXT} />
                    </Pressable>

                    {/* <Text style={textRegular}>
                            {i18n.t(Lang.RULES_TEXT_6)}
                        </Text> */}

                    <Text style={{ ...textHeader, color: ThemeColor.PRIMARY_TEXT }}>{i18n.t(Lang.RULES_TITLE_3)}</Text>
                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_7)}
                        <Text style={{ fontWeight: 'bold' }}>{i18n.t(Lang.RULES_TEXT_7_BOLD)}</Text>
                    </Text>

                    <Text style={textRegular}>
                        {i18n.t(Lang.RULES_TEXT_8)}
                    </Text>

                    {/* <Text style={{ ...textHeader, color: ThemeColor.PRIMARY_TEXT }}> Les mini-jeux </Text>

                    <Text style={{ ...textRegular, textAlign: 'center', fontFamily: 'RobotoSlab_700Bold' }}> Calcul Mental </Text>

                    <View style={styles.gameContainer}>
                        <TextInputLevel onChangeText={() => { }} onSubmit={() => { }} level={new CalculationLevelData(new Hash(0))} />
                    </View>

                    <Text style={textRegular}>
                        Est ce que je suis vraiment obliger d'expliquer ça ?... Oui ? Bon et bien voilà un calcul que tu dois résoudre.
                    </Text>

                    <Text style={{ ...textLight, textAlign: 'center' }}> (Si tu comprend pas ça tu n'es pas prêt pour la suite mon gars.. ) </Text> */}
                </View>

            </ScrollView >
        </View >
    )
}

const textHeader = {
    backgroundColor: ThemeColor.PRIMARY_SHADE,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    marginBottom: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 23,
    //fontFamily: 'RobotoSlab_700Bold',
}
const textLight = {
    fontSize: 12,
    //fontFamily: 'RobotoSlab_300Light',
    marginBottom: 10,
    color: ThemeColor.TERTIARY_TEXT
}
const textRegular = {
    marginHorizontal: 10,
    fontSize: 14,
    //fontFamily: 'RobotoSlab_400Regular',
    marginBottom: 10,
    color: ThemeColor.SECONDARY_TEXT
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 6,
        backgroundColor: ThemeColor.PRIMARY
    },
    scrollContainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    gameContainer: {
        marginHorizontal: 50,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: ThemeColor.PRIMARY_SHADE,
    },
    skipButtonContainer: {
        flexDirection: 'row',
    },
    skipButton: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: 70,
        paddingVertical: 5,
        marginBottom: 10,
        borderRadius: 35,
        backgroundColor: ThemeColor.BLUE,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

})

export default Rules