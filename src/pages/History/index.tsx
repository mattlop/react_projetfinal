import { useEffect, useRef, useState } from 'react'
import { ActivityIndicator, Modal, Pressable, ScrollView, Share, StyleSheet, Text, View } from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Entypo } from '@expo/vector-icons'
import { ThemeColor } from '../../../theme'
import { GameData } from '../../../types/types'
import { Results } from '../../components'
import { useStorage } from '../../hooks'
import { insertBeforeLast } from '../../../utils'
import React from 'react'
import i18n, { Lang } from '../../../locale'

const HistoryPage = ({ navigation, route }: any) => {
    const { gameDatas, gameDatasLoaded } = useStorage()
    const [activeGameDatas, setActiveGameDatas] = useState<number[]>([0])
    const [historyGameDatas, setHistoryGameDatas] = useState<GameData[]>([])
    const [scrollViewEnable, setScrollViewEnable] = useState<boolean>(false)
    useEffect(() => {
        if (route.params && route.params.insertHomeInNav) navigation.dispatch(insertBeforeLast('Home'))
    }, [])

    useEffect(() => {
        setHistoryGameDatas(([] as GameData[]).concat(gameDatas).reverse())
    }, [gameDatas])

    if (!gameDatasLoaded) {
        return (
            <View style={styles.mainContainerCenter}>
                <ActivityIndicator size={"large"} color={ThemeColor.WHITE} />
            </View>)
    }

    if (historyGameDatas.length === 0) {
        return (
            <View style={styles.mainContainerCenter}>
                <Text style={styles.textElement}>
                    {i18n.t(Lang.NO_PARTY_PLAYED)}
                </Text>
                <Text style={styles.textLight}>
                    {i18n.t(Lang.YOU_KNOW_WHAT_TO_DO)} ...
                </Text>
            </View>
        )
    }

    const onShare = async (gameData: GameData) => {
        try {
            let levelResultsEmojis = ""
            gameData.levels.forEach((level) => {
                levelResultsEmojis += level.displayableResult.points > 0 ? '🟩' : '🟥'
            })
            const result = await Share.share({
                message: `${i18n.t(Lang.APP_TITLE)}\nSCORE: ${gameData.score} | MODE: ${gameData?.mode?.name}\n${levelResultsEmojis}\nhttps://reactnativefinalproject-6f28c.web.app/`,
            })
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
            return
        } catch (error: any) {
            console.log(error.message)
            return
        }
    }

    const _renderSectionTitle = (gameData: GameData) => {
        return null;
    };

    const _renderHeader = (gameData: GameData) => {
        return (

            <View style={styles.titleContainer}>
                <Pressable style={styles.shareButton} onPress={() => onShare(gameData)}>
                    <Entypo name="share" size={30} color={ThemeColor.BLUE} />
                </Pressable>
                <Text style={styles.titleText} adjustsFontSizeToFit numberOfLines={1}>{gameData.name}</Text>
                <View style={styles.scoreContainer}>
                    <Text style={styles.scoreText}>SCORE</Text>
                    <Text style={styles.scorePoints}>{gameData.score}</Text>
                </View>
            </View>
        );
    };

    const disableScrollView = () => {
        setScrollViewEnable(false)
    }

    const enableScrollView = () => {
        setScrollViewEnable(true)
    }

    const _renderContent = (gameData: GameData) => {
        return (
            <Results game={gameData} onDragStart={disableScrollView} onDragEnd={enableScrollView} />
        );
    };

    const _updateSections = (activeGameDatas: number[]) => {
        setActiveGameDatas(activeGameDatas);
    };

    return (
        <SafeAreaView onTouchEnd={() => { enableScrollView() }} style={styles.mainContainer}>
            <ScrollView scrollEnabled={scrollViewEnable} showsVerticalScrollIndicator={false}>
                <Accordion
                    sections={historyGameDatas}
                    activeSections={activeGameDatas}
                    renderHeader={_renderHeader}
                    renderContent={_renderContent}
                    onChange={_updateSections}
                    underlayColor={""}
                    sectionContainerStyle={styles.sectionContainer}
                    containerStyle={styles.accordionContainer}
                    expandMultiple
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    accordionContainer: {
        paddingHorizontal: 10
    },
    sectionContainer: {
        marginVertical: 10,
        paddingHorizontal: 0,
        paddingVertical: 0,
        overflow: 'hidden',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        borderRadius: 20,
        backgroundColor: ThemeColor.PRIMARY,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 1,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    titleContainer: {
        alignSelf: 'center',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        borderBottomColor: ThemeColor.PRIMARY,
        borderBottomWidth: 1,
        overflow: 'hidden',
    },
    titleText: {
        flex: 1,
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        textAlign: 'center',
        paddingVertical: 3,
        paddingHorizontal: 6,
    },
    shareButton: {
        // paddingHorizontal: 5
    },
    scoreContainer: {
        alignSelf: 'center',
        width: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'

    },
    scoreText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 3,
        paddingHorizontal: 8,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    },
    scorePoints: {
        backgroundColor: ThemeColor.BLUE,
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 3,
        paddingHorizontal: 10,
    },
    mainContainerCenter: {
        backgroundColor: ThemeColor.PRIMARY,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainContainer: {
        backgroundColor: ThemeColor.PRIMARY,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch'
    },
    textElement: {
        fontSize: 20,
        //fontFamily: 'RobotoSlab_600SemiBold',
        color: ThemeColor.PRIMARY_TEXT,
    },
    textLight: {
        fontSize: 12,
        //fontFamily: 'RobotoSlab_300Light',
        marginBottom: 10,
        marginTop: 5,
        color: ThemeColor.PRIMARY_TEXT,
    },
    scoreElement: {
        fontSize: 23,
        //fontFamily: 'RobotoSlab_700Bold',
        color: ThemeColor.PRIMARY_TEXT,
    }
})

export default HistoryPage