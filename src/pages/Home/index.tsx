import { useIsFocused } from '@react-navigation/native'
import { Entypo } from '@expo/vector-icons'
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons'
import { MaterialIcons } from '@expo/vector-icons'
import { useEffect, useState } from 'react'
import { Keyboard, Pressable, StyleSheet, Switch, Text, View } from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import * as NavigationBar from 'expo-navigation-bar'
import i18n, { Lang } from '../../../locale'
import { ThemeColor } from '../../../theme'
import { generateColorByTimestamp } from '../../../utils'
import { ProfileIcon, TimerBar } from '../../components'
import { TextInput } from 'react-native-gesture-handler'
import { gameModes } from '../../../types/types'
import * as React from 'react'

const Home = ({ navigation }: any) => {
    const [showFirstTimer, setShowFirstTimer] = useState<boolean>(true)
    const [selectedGameModeIndex, setSelectedGameModeIndex] = useState<number>(0)
    const [isTrainingMode, setIsTrainingMode] = useState<boolean>(false)
    const [currentColor, setCurrentColor] = useState<string>(ThemeColor.WHITE)
    const [tag, setTag] = useState<string>("")

    const [isKeyboardVisible, setKeyboardVisible] = useState(false);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setKeyboardVisible(true); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardVisible(false); // or some other action
            }
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, []);

    const timerFinished = () => { changeHash() }
    const firstTimerFinished = () => { setShowFirstTimer(false); changeHash() }
    //const { ready, setOwnUserName, getScoreboard, setOwnScoreInScoreboard, getUsername, getUsernames } = useFireStore()

    const secFirstBar = 60 - new Date(Date.now()).getSeconds()
    const isFocused = useIsFocused();

    const gameModesData = gameModes.map((gameMode, index) => {
        return { key: index, label: gameMode.name }
    })

    const goToGame = () => {
        navigation.navigate('Game', { gameModeIndex: selectedGameModeIndex, tag: tag, training: isTrainingMode })
    }

    const changeHash = () => {
        if (isFocused) {
            setCurrentColor(generateColorByTimestamp(Date.now()))
        }
    };

    useEffect(() => {
        if (isFocused) {
            changeHash()
        }
    }, [isFocused])

    return (
        <View style={styles.container}>
            <ProfileIcon navigation={navigation} />

            <View style={styles.titleContainer}>
                <Text style={styles.titleText}>
                    {i18n.t(Lang.APP_TITLE)}
                </Text>

                <View style={styles.barContainer}>
                    {showFirstTimer && (
                        <TimerBar timerElapsed={firstTimerFinished} maxSeconds={60} seconds={secFirstBar} autoRestart={false} />
                    )}
                    {!showFirstTimer && (
                        <TimerBar timerElapsed={timerFinished} seconds={60} autoRestart={true} />
                    )}
                </View>
            </View>

            <View style={styles.mainContainer}>

                <View style={styles.playContainer}>
                    <Pressable style={styles.playButton} onPress={() => { goToGame() }} >
                        <Entypo style={styles.buttonIcon} name="controller-play" size={80} color={currentColor} />
                        <Text style={styles.playButtonText}>{i18n.t(Lang.PLAY)}</Text>
                    </Pressable>
                </View>

                <View style={styles.gameModeContainer}>
                    <View style={styles.gameModeCardContainer}>
                        <View style={styles.gameModeHeader}>
                            <View style={styles.gameModeHeaderLeft}>
                                <Ionicons style={styles.gameModeIcon} name="game-controller" size={24} color={ThemeColor.WHITE} />
                                <Text style={styles.gameModeHint}>{i18n.t(Lang.GAME_MODE)}</Text>
                            </View>

                            <View style={styles.gameModeHeaderRight}>
                                <ModalSelector
                                    data={gameModesData}
                                    initValue={gameModesData[0].label}
                                    style={styles.gameModeSelect}
                                    backdropPressToClose
                                    overlayStyle={styles.gameModeOptionsOverlay}
                                    optionContainerStyle={styles.gameModeOptionContainer}
                                    header={
                                        <Text style={styles.gameModeOptionsHeader}>
                                            {i18n.t(Lang.GAME_MODE)}
                                        </Text>
                                    }
                                    optionStyle={styles.gameModeOption}
                                    optionTextStyle={styles.gameModeOptionText}
                                    cancelText=''
                                    cancelStyle={{ backgroundColor: '' }}
                                    animationType='none'
                                    onScroll={
                                        () => NavigationBar.setBackgroundColorAsync(ThemeColor.PRIMARY)
                                    }
                                    onChange={option => setSelectedGameModeIndex(option.key)} >
                                    <View style={styles.gameModeTitle}>
                                        <Text style={styles.gameModeSelectText}>
                                            {gameModes[selectedGameModeIndex].name}
                                        </Text>
                                        <Entypo name="chevron-down" size={24} color={ThemeColor.WHITE} />
                                    </View>
                                </ModalSelector>
                            </View>
                        </View>

                        <View style={styles.gameModeContent}>
                            {gameModes[selectedGameModeIndex] && (
                                <Text style={styles.gameModeDescription}>{gameModes[selectedGameModeIndex].description}</Text>
                            )}
                        </View>

                        <View style={styles.gameModeTrainingContainer}>
                            <Switch value={isTrainingMode}
                                onValueChange={value => setIsTrainingMode(value)}
                                thumbColor={isTrainingMode ? currentColor : ThemeColor.PRIMARY_SHADE}
                                trackColor={{ true: currentColor + '50', false: ThemeColor.PRIMARY_THIN }} />
                            <Text style={styles.gameModeTrainingText}>{i18n.t(Lang.TRAINING)}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.tagContainer}>
                    <View style={styles.tag}>
                        <Text style={styles.tagText}>#</Text>
                        <TextInput style={styles.tagInput}
                            onChangeText={(text) => setTag(text)}
                            value={tag}
                            placeholder={i18n.t(Lang.TAG_PLACEHOLDER)} placeholderTextColor={ThemeColor.PRIMARY} />
                    </View>
                </View>

            </View>
            {!isKeyboardVisible && (
                <View style={styles.menuContainer}>
                    <Pressable style={styles.menuButton} onPress={() => { navigation.push('History') }} >
                        <FontAwesome5 style={styles.buttonIcon} name="history" size={30} color={ThemeColor.WHITE} />
                        <Text style={styles.menuButtonText}>{i18n.t(Lang.HISTORY)}</Text>
                    </Pressable>
                    <Pressable style={styles.menuButton} onPress={() => { navigation.push('Ranking') }} >
                        <MaterialIcons style={styles.buttonIcon} name="leaderboard" size={30} color={ThemeColor.WHITE} />
                        <Text style={styles.menuButtonText}>{i18n.t(Lang.RANKING)}</Text>
                    </Pressable>
                    <Pressable style={styles.menuButton} onPress={() => { navigation.push('Rules') }} >
                        <Entypo style={styles.buttonIcon} name="info" size={30} color={ThemeColor.WHITE} />
                        <Text style={styles.menuButtonText}>{i18n.t(Lang.RULES)}</Text>
                    </Pressable>
                </View>
            )}

        </View>)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ThemeColor.PRIMARY,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    titleContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleText: {
        fontSize: 32,
        margin: 10,
        //fontFamily: 'RobotoSlab_900Black',
        color: ThemeColor.PRIMARY_TEXT,
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    barContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainContainer: {
        flex: 10,
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    playContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    playButton: {
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 42,
        paddingRight: 38,
        paddingTop: 0,
        paddingBottom: 6,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.37,
        shadowRadius: 3.49,

        elevation: 4,
    },
    playButtonText: {
        color: ThemeColor.PRIMARY_LIGHT,
        fontSize: 30,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        alignSelf: 'center'
    },
    gameModeContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    gameModeCardContainer: {
        //flex: 1,
        width: '95%',
        margin: 10,
        //overflow: 'hidden',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        backgroundColor: ThemeColor.PRIMARY,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    gameModeHeader: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        borderBottomColor: ThemeColor.PRIMARY,
        borderBottomWidth: 1,
        zIndex: 2,
    },
    gameModeHeaderLeft: {
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingVertical: 2,
        paddingHorizontal: 10,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1
    },
    gameModeHeaderRight: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    gameModeIcon: {
    },
    gameModeTitle: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'space-between',
    },
    gameModeHint: {
        textAlign: 'center',
        color: ThemeColor.PRIMARY_LIGHT,
        textTransform: 'uppercase',
        fontSize: 8,
    },
    gameModeSelect: {
        backgroundColor: 'transparent'
    },
    gameModeSelectText: {
        flex: 1,
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderWidth: 0,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 16,
    },
    gameModeOptionsOverlay: {
        flex: 1,
        width: '100%',
        margin: 0,
        padding: 0,
        paddingTop: 50,
        paddingHorizontal: 50,
        alignSelf: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#ffffff10',
    },
    gameModeOptionContainer: {
        height: '40%',
        borderWidth: 0,
        padding: 0,
        paddingBottom: 10,
        margin: 0,
        backgroundColor: ThemeColor.PRIMARY_LIGHT,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    gameModeOptionsHeader: {
        paddingVertical: 10,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 22,
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    gameModeOption: {
        borderBottomWidth: 0,
        paddingHorizontal: 0,
        paddingBottom: 0,
        margin: 0
    },
    gameModeOptionText: {
        borderWidth: 0,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 16,
        paddingVertical: 10,
        backgroundColor: ThemeColor.PRIMARY,
        borderRadius: 5,
    },
    gameModeContent: {
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        zIndex: 1,
        overflow: 'hidden'
    },
    gameModeDescription: {
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: ThemeColor.PRIMARY_LIGHT,
        fontSize: 16,
        paddingHorizontal: 10,
    },
    gameModeTrainingContainer: {
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY,
        //paddingVertical: 5,
        paddingHorizontal: 15,
        borderTopColor: ThemeColor.PRIMARY_THIN,
        borderTopWidth: 1,
        borderRadius: 20,
    },
    gameModeTrainingText: {
        textAlign: 'center',
        textTransform: 'uppercase',
        color: ThemeColor.PRIMARY_THIN,
        fontWeight: 'bold',
        fontSize: 16,
        paddingHorizontal: 10,
    },
    tagContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    tag: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        paddingHorizontal: 10,
        paddingTop: 3,
        paddingBottom: 6,
        borderRadius: 10,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    tagText: {
        color: ThemeColor.PRIMARY_SHADE,
        fontSize: 20,
        fontWeight: 'bold',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        textAlign: 'center',
    },
    tagInput: {
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 20,
        paddingHorizontal: 5,
        backgroundColor: ThemeColor.PRIMARY_THIN,
        textAlign: 'left'
    },
    menuContainer: {
        flex: 2,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    menuButton: {
        flex: 1,
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16,
    },
    buttonIcon: {
        //alignSelf: 'flex-start',
    },
    menuButtonText: {
        color: ThemeColor.PRIMARY_LIGHT,
        fontSize: 12,
        textTransform: 'uppercase',
        padding: 6,
        //fontFamily: 'RobotoSlab_600SemiBold',
        alignSelf: 'center'
    },
})

export default Home