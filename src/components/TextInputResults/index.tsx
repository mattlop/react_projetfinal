import { MaterialCommunityIcons } from '@expo/vector-icons'
import { FontAwesome5 } from '@expo/vector-icons'
import { FontAwesome } from '@expo/vector-icons'
import { Entypo } from '@expo/vector-icons'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ThemeColor } from '../../../theme'
import { LevelData } from '../../../types'
import { getDisplayableTime } from '../../../utils'

type TextInputResultsProps = {
    index: number,
    level: LevelData,
}

const TextInputResults = (displayedResult: TextInputResultsProps) => {


    return (<View style={styles.displayedLevelResult}>
        <View style={styles.displayedLevelResultHeader}>
            <Text style={getDisplayedLevelIndexStyle(displayedResult.level)}>{displayedResult.index + 1}</Text>
            <Text style={styles.displayedLevelType}>{displayedResult.level.levelType}</Text>
        </View>
        <View style={styles.displayedLevelResultBody}>
            <View style={styles.displayedLevelResultBodyLeft}>
                <View style={styles.displayedLevelResultInfo}>
                    <MaterialCommunityIcons name='timer' size={24} color={ThemeColor.YELLOW} />
                    <Text style={styles.displayedLevelResultText} numberOfLines={1}>{getDisplayableTime(displayedResult.level.responseTime)}s</Text>
                </View>
                <View style={styles.displayedLevelResultInfo}>
                    <Entypo name='clipboard' size={24} color={ThemeColor.YELLOW} />
                    <Text style={styles.displayedLevelResultText} numberOfLines={1}>{displayedResult.level.displayableResult.statement}</Text>
                </View>
            </View>
            <View style={styles.displayedLevelResultBodyRight}>
                <View style={styles.displayedLevelResultInfo}>
                    <FontAwesome5 name='pencil-alt' size={24} color={displayedResult.level.displayableResult.points > 0 ? ThemeColor.SUCCESS : ThemeColor.DANGER} />
                    <Text style={styles.displayedLevelResultText} numberOfLines={1}>{displayedResult.level.displayableResult.userInput}</Text>
                </View>
                <View style={styles.displayedLevelResultInfo}>
                    <FontAwesome name='check' size={24} color={ThemeColor.YELLOW} />
                    <Text style={styles.displayedLevelResultText} numberOfLines={1}>{displayedResult.level.displayableResult.result}</Text>
                </View>
            </View>
        </View>
    </View>)
}

const styles = StyleSheet.create({
    displayedLevelResult: {
        width: '100%',
        justifyContent: 'space-evenly',
        alignSelf: 'stretch',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'
    },
    displayedLevelResultHeader: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: ThemeColor.PRIMARY,
        flexDirection: 'row',
        overflow: 'hidden'
    },
    displayedLevelType: {
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 20,
        paddingVertical: 6,
        paddingHorizontal: 12,
        textTransform: 'uppercase'
    },
    displayedLevelResultBody: {
        width: '100%',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    displayedLevelResultBodyLeft: {
        flex: 4,
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    displayedLevelResultBodyRight: {
        flex: 3,
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    displayedLevelResultInfo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingVertical: 10
    },
    displayedLevelResultText: {
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 5,
        paddingHorizontal: 10,
        overflow: 'hidden',
    },
})

const getDisplayedLevelIndexStyle = (level: LevelData): any => {
    return {
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
        paddingVertical: 5,
        paddingHorizontal: 16,
        backgroundColor: level.displayableResult.points > 0 ? ThemeColor.SUCCESS : ThemeColor.DANGER,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    }
}

export default TextInputResults;