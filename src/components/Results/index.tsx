import React from 'react'
import { useEffect, useState } from 'react'
import { Pressable, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import i18n, { Lang } from '../../../locale'
import { ThemeColor } from '../../../theme'
import { LevelData } from '../../../types'
import GeometryFinderLevelData from '../../../types/GeometryFinderLevelData'
import { LevelType } from '../../../types/LevelType'
import { GameData } from '../../../types/types'
import GeometryFinderResults from '../GeometryFinderResults'
import TextInputResults from '../TextInputResults'

type ResultsProps = {
    game: GameData
    onDragStart?: () => void,
    onDragEnd?: () => void,
}

const Results = (props: ResultsProps) => {
    const { game, onDragStart, onDragEnd } = props
    const [displayedResult, setDisplayedResult] = useState<{ index: number, level: LevelData }>()

    useEffect(() => {
        setDisplayedResult({ index: 0, level: game.levels[0] })
    }, [game])

    return (

        <View /*style={styles.container}*/>
            {/* <View style={styles.titleContainer}>
                <Text style={styles.titleText} adjustsFontSizeToFit numberOfLines={1}>{game.name}</Text>
            </View> */}
            <View style={styles.gameInfosContainer}>
                <View style={styles.gameInfoContainer}>
                    <Text style={styles.gameInfoText}>{i18n.t(Lang.MODE)}</Text>
                    <Text style={styles.gameInfoValue}>{game?.mode?.name}</Text>
                </View>
                <View style={styles.gameInfoContainer}>
                    <Text style={styles.gameInfoText}>{i18n.t(Lang.TAG)}</Text>
                    <Text style={styles.gameInfoValue}>{game.tag}</Text>
                </View>
            </View>
            <View style={styles.resultContainer}>
                <View style={styles.resultLeftContainer}>
                    <SafeAreaView onTouchStart={() => { if (onDragStart) { onDragStart() } }} onTouchEnd={() => { if (onDragEnd) { onDragEnd() } }} style={styles.safeAreaView}>
                        <ScrollView onScrollEndDrag={() => { if (onDragEnd) { onDragEnd() } }} showsVerticalScrollIndicator={false}>
                            {game.levels.map((level, index) => (
                                <View key={index}>
                                    <Pressable onPress={() => setDisplayedResult({ index, level })} style={getLevelBubbleStyle(level, displayedResult?.level)}>
                                        <Text style={getLevelBubbleTextStyle(level, displayedResult?.level)}>{index + 1}</Text>
                                    </Pressable>
                                </View>
                            ))}
                        </ScrollView>
                    </SafeAreaView>
                </View>
                <View style={styles.resultRightContainer}>
                    {displayedResult?.level &&
                        (displayedResult.level.levelType == LevelType.BIN_TO_DEC ||
                            displayedResult.level.levelType == LevelType.CALCUL ||
                            displayedResult.level.levelType == LevelType.HEX_TO_DEC) &&
                        <TextInputResults index={displayedResult.index} level={displayedResult.level} />
                    }
                    {displayedResult?.level &&
                        (displayedResult.level.levelType == LevelType.GEOMETRY_FINDER) &&
                        <GeometryFinderResults index={displayedResult.index} level={(displayedResult.level as GeometryFinderLevelData)} />
                    }

                    {/* <View style={styles.bottomContainer}>
                        <View style={styles.shareButton}>
                            <Entypo name="share" size={40} color={ThemeColor.BLUE} />
                        </View>
                        <View style={styles.scoreContainer}>
                            <Text style={styles.scoreText}>SCORE</Text>
                            <Text style={styles.scorePoints}>{game.score}</Text>
                        </View>
                    </View> */}
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '90%',
        margin: 10,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 20,
        backgroundColor: ThemeColor.PRIMARY,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    titleContainer: {
        alignSelf: 'center',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        borderBottomColor: ThemeColor.PRIMARY,
        borderBottomWidth: 1,
        overflow: 'hidden',
    },
    titleText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 30,
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    gameInfosContainer: {
        alignSelf: 'center',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingTop: 14
    },
    gameInfoContainer: {
        alignSelf: 'center',
        width: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        marginRight: 10,
        backgroundColor: ThemeColor.PRIMARY,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'

    },
    gameInfoText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 3,
        paddingHorizontal: 8,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    },
    gameInfoValue: {
        backgroundColor: ThemeColor.PRIMARY_THIN,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 3,
        paddingHorizontal: 10,
    },
    resultContainer: {
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-evenly',
    },
    resultLeftContainer: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    resultRightContainer: {
        width: '70%',
        height: '100%',
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        paddingVertical: 16
    },
    bottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        paddingVertical: 20
    },
    shareButton: {
        paddingHorizontal: 10
    },
    scoreContainer: {
        alignSelf: 'center',
        width: 'auto',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'

    },
    scoreText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 24,
        paddingVertical: 6,
        paddingHorizontal: 12,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    },
    scorePoints: {
        backgroundColor: ThemeColor.BLUE,
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 24,
        paddingVertical: 6,
        paddingHorizontal: 20,
    },
    safeAreaView: {
        margin: 10,
        height: 180
    },
    displayedLevelResult: {
        width: '100%',
        justifyContent: 'space-evenly',
        alignSelf: 'stretch',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'
    },
    displayedLevelResultHeader: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: ThemeColor.PRIMARY,
        flexDirection: 'row',
        overflow: 'hidden'
    },
    displayedLevelType: {
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 24,
        paddingVertical: 6,
        paddingHorizontal: 12,
        textTransform: 'uppercase'
    },
    displayedLevelResultBody: {
        width: '100%',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    displayedLevelResultBodyLeft: {
        flex: 4,
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    displayedLevelResultBodyRight: {
        flex: 3,
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    displayedLevelResultInfo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingVertical: 10
    },
    displayedLevelResultText: {
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 5,
        paddingHorizontal: 10,
        overflow: 'hidden',
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    }
})

const getLevelBubbleStyle = (level: LevelData, displayedLevel?: LevelData): any => {
    let backgroundColor: string

    if (level && level.displayableResult.points > 0) {
        if (level === displayedLevel) backgroundColor = ThemeColor.PRIMARY_TEXT // white
        else backgroundColor = ThemeColor.SUCCESS // green
    } else {
        if (level === displayedLevel) backgroundColor = ThemeColor.PRIMARY_TEXT // white
        else backgroundColor = ThemeColor.DANGER // red
    }

    return {
        width: 40,
        height: 40,
        borderRadius: 50,
        margin: 5,
        padding: 5,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor
    }
}

const getLevelBubbleTextStyle = (level: LevelData, displayedLevel?: LevelData): any => {
    let color: string

    if (level && level.displayableResult.points > 0) {
        if (level === displayedLevel) color = ThemeColor.SUCCESS // green
        else color = ThemeColor.PRIMARY_TEXT // white
    } else {
        if (level === displayedLevel) color = ThemeColor.DANGER // red
        else color = ThemeColor.PRIMARY_TEXT // white
    }

    return {
        fontWeight: 'bold',
        fontSize: 18,
        color
    }
}

const getDisplayedLevelIndexStyle = (level: LevelData): any => {
    return {
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
        paddingVertical: 5,
        paddingHorizontal: 16,
        backgroundColor: level.displayableResult.points > 0 ? ThemeColor.SUCCESS : ThemeColor.DANGER,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    }
}

export default Results