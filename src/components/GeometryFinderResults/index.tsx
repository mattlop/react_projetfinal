import { MaterialCommunityIcons } from '@expo/vector-icons'
import { FontAwesome5 } from '@expo/vector-icons'
import React from 'react'
import { useEffect } from 'react'
import { StyleSheet, Text, View, Image, ActivityIndicator } from 'react-native'
import { ThemeColor } from '../../../theme'
import { LevelData, GeometryFinderLevelData } from '../../../types'
import { GeometryFinderShape } from '../../../types/GeometryFinderLevelData'
import { getDisplayableTime } from '../../../utils'

type GeometryFinderResultsProps = {
    index: number,
    level: GeometryFinderLevelData,
}

const GeometryFinderResults = (props: GeometryFinderResultsProps) => {
    const { level, index } = props;

    const square = require('../../../assets/img/square.png');
    const circle = require('../../../assets/img/circle.png');
    const triangle = require('../../../assets/img/triangle.png');

    const getImgByShape = (shape: GeometryFinderShape): NodeRequire => {
        switch (shape) {
            case (GeometryFinderShape.CIRCLE):
                return circle;
            case (GeometryFinderShape.SQUARE):
                return square;
            case (GeometryFinderShape.TRIANGLE):
                return triangle;
        }
    }

    useEffect(() => {

    }, [level])

    if (!level) {
        return (<ActivityIndicator size={"large"} color={ThemeColor.WHITE} />)
    }

    return (<View style={styles.displayedLevelResult}>
        <View style={styles.displayedLevelResultHeader}>
            <Text style={getDisplayedLevelIndexStyle(level)}>{index + 1}</Text>
            <Text style={styles.displayedLevelType} numberOfLines={1}>{level.levelType}</Text>
        </View>
        <View style={styles.displayedLevelResultBody}>
            <View style={styles.displayedLevelResultBodyLeft}>
                <View style={styles.displayedLevelResultInfo}>
                    <MaterialCommunityIcons name='timer' size={24} color={ThemeColor.YELLOW} />
                    <Text style={styles.displayedLevelResultText} numberOfLines={1}>{getDisplayableTime(level.responseTime)}s</Text>
                </View>
                <View style={styles.displayedLevelResultInfo}>
                    <FontAwesome5 name='pencil-alt' size={24} color={level.displayableResult.points > 0 ? ThemeColor.SUCCESS : ThemeColor.DANGER} />
                    <View style={styles.shapeContainerUserInput}>
                        <View style={styles.shapeContainer}>
                            <View style={styles.shape}>
                                <Image style={styles.bigImg} source={getImgByShape(GeometryFinderLevelData.UserInputToShapes(level)[0])} />
                            </View>
                            <View style={styles.shape}>
                                <Image style={styles.smallImg} source={getImgByShape(GeometryFinderLevelData.UserInputToShapes(level)[1])} />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.displayedLevelResultBodyRight}>
                {level.tabshapes.map((rowFigure, y_index) => (
                    <View key={y_index} style={styles.rowFigureContainer}>
                        {rowFigure.map((figure, x_index) => (
                            <View key={`${y_index}-${x_index}`} style={styles.shapeContainer}>
                                {!GeometryFinderLevelData.isHideFigureStatic(x_index, y_index, level) && (
                                    <View style={styles.shapeContainer}>
                                        <View style={styles.shape}>
                                            <Image style={styles.bigImg} source={getImgByShape(figure.shapes[0])} />
                                        </View>
                                        <View style={styles.shape}>
                                            <Image style={styles.smallImg} source={getImgByShape(figure.shapes[1])} />
                                        </View>
                                    </View>
                                )}
                                {GeometryFinderLevelData.isHideFigureStatic(x_index, y_index, level) && (
                                    <View style={styles.shapeContainerHide}>
                                        <View style={styles.shape}>
                                            <Image style={styles.bigImg} source={getImgByShape(figure.shapes[0])} />
                                        </View>
                                        <View style={styles.shape}>
                                            <Image style={styles.smallImg} source={getImgByShape(figure.shapes[1])} />
                                        </View>
                                    </View>
                                )}
                            </View>
                        ))}
                    </View>
                ))}
            </View>
        </View>
    </View>)
}

const styles = StyleSheet.create({
    displayedLevelResult: {
        width: '100%',
        justifyContent: 'space-evenly',
        alignSelf: 'stretch',
        alignItems: 'center',
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        overflow: 'hidden'
    },
    displayedLevelResultHeader: {
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: ThemeColor.PRIMARY,
        flexDirection: 'row',
        overflow: 'hidden'
    },
    displayedLevelType: {
        alignSelf: 'center',
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 20,
        paddingVertical: 6,
        paddingHorizontal: 12,
        textTransform: 'uppercase'
    },
    displayedLevelResultBody: {
        width: '100%',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    displayedLevelResultBodyLeft: {
        flex: 4,
        alignItems: 'flex-start',
        justifyContent: 'space-evenly',
    },
    displayedLevelResultBodyRight: {
        alignSelf: 'stretch',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    rowFigureContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    shapeContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    shapeContainerUserInput: {
        marginStart: 25,
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    shapeContainerHide: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ThemeColor.BLUE,
    },

    shape: {
        position: 'absolute'
    },
    bigImg: {
        width: 30,
        height: 30,
    },
    smallImg: {
        width: 8,
        height: 8,
    },


    displayedLevelResultInfo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        paddingVertical: 10
    },
    displayedLevelForm: {
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',

    },
    displayedLevelResultText: {
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 18,
        paddingVertical: 5,
        paddingHorizontal: 10,
        overflow: 'hidden',
    },
})

const getDisplayedLevelIndexStyle = (level: LevelData): any => {
    return {
        color: ThemeColor.PRIMARY_TEXT,
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
        paddingVertical: 5,
        paddingHorizontal: 16,
        backgroundColor: level.displayableResult.points > 0 ? ThemeColor.SUCCESS : ThemeColor.DANGER,
        borderRightColor: ThemeColor.PRIMARY,
        borderRightWidth: 1,
    }
}

export default GeometryFinderResults;