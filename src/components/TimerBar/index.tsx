import * as React from 'react'
import { Subscription, interval } from 'rxjs'
import { StyleSheet, View } from 'react-native'
import { useEffect, useState } from 'react'
import { ThemeColor } from '../../../theme'

type TimerBarProps = {
    seconds: number,
    timerElapsed: () => void,
    autoRestart: boolean,
    maxSeconds?: number
}

const TimerBar = (props: TimerBarProps) => {
    const { seconds, timerElapsed, autoRestart, maxSeconds } = props

    const startSec = maxSeconds ? maxSeconds : seconds

    const [isTimerElapsed, setIsTimerElapsed] = useState<boolean>(false)
    const [remain, setRemain] = useState<number>(0)
    let timerSub: Subscription

    useEffect(() => {
        let timeStampEnd = generateTimeStampEnd(seconds)
        let dateStart = new Date(Date.now())
        dateStart = new Date(dateStart.setSeconds(dateStart.getSeconds() - (startSec - seconds)))
        let timeStampStart = dateStart.getTime()
        const timer = interval(100)
        timerSub = timer.subscribe(() => {
            if (!isTimerElapsed) {
                const result = (((timeStampEnd - Date.now()) * 100) / (timeStampEnd - timeStampStart))
                if (result <= 0) {
                    if (!autoRestart) {
                        timerSub.unsubscribe()
                    } else {
                        timeStampEnd = generateTimeStampEnd(seconds)
                        timeStampStart = Date.now()
                    }
                    setIsTimerElapsed(true)
                } else {
                    setRemain(200 - result * 2)
                }
            }
        })
        return () => {
            timerSub.unsubscribe()
        }
    }, [])

    useEffect(() => {
        if (isTimerElapsed) {
            timerElapsed()
            if (autoRestart) {
                setIsTimerElapsed(false)
            }
        }
    }, [isTimerElapsed])

    return (
        <View style={styles.barContainer}>
            <View style={{ backgroundColor: ThemeColor.WHITE, alignSelf: 'flex-start', flex: 1, width: remain, borderRadius: 5 }} />
        </View>)
}

const generateTimeStampEnd = (seconds: number): number => {
    const dateNow: Date = new Date(Date.now())
    dateNow.setSeconds(dateNow.getSeconds() + seconds)
    return dateNow.getTime()
}

const styles = StyleSheet.create({
    barContainer: {
        width: 200,
        height: 6,
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        borderRadius: 5,
        alignContent: 'flex-start',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
})

export default TimerBar