import React, { useEffect, useState } from 'react'
import { Pressable, ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import { ThemeColor } from '../../../theme';
import { useFireBaseAuth, useFireStore } from '../../hooks';
import { AntDesign } from '@expo/vector-icons';
import i18n, { Lang } from '../../../locale';

type ProfileIconProps = {
    navigation: any,
    disabled?: boolean,
}

const ProfileIcon = (props: ProfileIconProps) => {
    const { navigation, disabled } = props
    const pressableDisable = disabled ? disabled : false
    const { getUsername, ready, user } = useFireStore()
    const [username, setUsername] = useState<string>("")

    useEffect(() => {
        if (ready) {
            getUsername(user!.uid).then(
                (un) => setUsername(un)
            ).catch(reason => {
                if (user) setUsername(`${user.email?.split('.')[0]}`)
                else setUsername("Guest")
            }
            )
        }
    }, [ready])

    return (
        <View style={styles.profilContainer}>
            <Text style={styles.profileText}>
                {ready == true ? username : i18n.t(Lang.LOGIN)}
            </Text>
            <Pressable disabled={pressableDisable} style={styles.profilIcon} onPress={() => { navigation.navigate("Profile") }}>
                {ready == null && (
                    <ActivityIndicator size={"large"} color={ThemeColor.WHITE} />
                )}
                {ready == false && (
                    <AntDesign name="login" size={24} color={ThemeColor.WHITE} />
                )}
                {ready == true && (
                    <AntDesign name="user" size={24} color={ThemeColor.WHITE} />
                )}
            </Pressable>
        </View>);
}

const styles = StyleSheet.create({
    profilContainer: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    profileText: {
        fontSize: 18,
        color: ThemeColor.PRIMARY_LIGHT,
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    profilIcon: {
        width: 60,
        height: 60,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default ProfileIcon;