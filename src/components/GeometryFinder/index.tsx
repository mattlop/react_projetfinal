import { Pressable, StyleSheet, Image, View } from 'react-native'
import { useEffect, useState } from 'react'
import { ThemeColor } from '../../../theme'
import React from 'react'
import GeometryFinderLevelData, { GeometryFinderShape } from '../../../types/GeometryFinderLevelData'
import { AntDesign } from '@expo/vector-icons';

type GeometryFinderProps = {
    level: GeometryFinderLevelData,
    onChangeChoice: (text: string) => void,
    onSubmit: () => void
}

const GeometryFinder = (props: GeometryFinderProps) => {
    const { level, onChangeChoice, onSubmit } = props

    const [bigSelected, setBigSelected] = useState<GeometryFinderShape | undefined>()
    const [smallSelected, setSmallSelected] = useState<GeometryFinderShape | undefined>()

    const square = require('../../../assets/img/square.png');
    const circle = require('../../../assets/img/circle.png');
    const triangle = require('../../../assets/img/triangle.png');

    const getImgByShape = (shape: GeometryFinderShape): NodeRequire => {
        switch (shape) {
            case (GeometryFinderShape.CIRCLE):
                return circle;
            case (GeometryFinderShape.SQUARE):
                return square;
            case (GeometryFinderShape.TRIANGLE):
                return triangle;
        }
    }

    const changeUserInput = (big?: GeometryFinderShape, small?: GeometryFinderShape) => {
        if (big !== undefined) {
            setBigSelected(big)
        }
        if (small !== undefined) {
            setSmallSelected(small)
        }
    }

    useEffect(() => {
        const userChoice =
            (bigSelected !== undefined ? bigSelected!.toString() : "-1") +
            ";" +
            (smallSelected !== undefined ? smallSelected!.toString() : "-1")

        onChangeChoice(userChoice)
        if (bigSelected !== undefined && smallSelected !== undefined) {

            /*console.log("GeometryFinder : " + (level.getResult() == userChoice ? "SUCCESS" : "FAILED"))
            console.log("---")
            console.log("userChoice : " + userChoice)
            console.log("result     : " + level.getResult())
            
            if(level.getResult() == userChoice){
                console.log(" ")
                for(let y = 0; y < level.tabshapes.length; ++y){
                    let log = "| "
                    for(let x = 0; x < level.tabshapes[y].length; ++x){
                        log += level.tabshapes[y][x].shapes[0].toString() + ';' + level.tabshapes[y][x].shapes[1].toString() + " | "
                    }
                    console.log(log)

                    if(y+1 < level.tabshapes.length) console.log("-------------------")
                }
            }

            console.log(" ")*/
            setBigSelected(undefined)
            setSmallSelected(undefined)
            onSubmit()
        }
    }, [smallSelected, bigSelected])

    return (
        <View style={styles.container}>
            <View style={styles.figuresContainer}>
                {level.tabshapes.map((rowFigure, y_index) => (
                    <View key={y_index} style={styles.rowFigureContainer}>
                        {rowFigure.map((figure, x_index) => (
                            <View key={`${y_index}-${x_index}`} style={styles.shapeContainer}>
                                {!level.isHideFigure(x_index, y_index) && (
                                    <View style={styles.shapeContainer}>
                                        <View style={styles.shape}>
                                            <Image style={styles.bigImg} source={getImgByShape(figure.shapes[0])} />
                                        </View>
                                        <View style={styles.shape}>
                                            <Image style={styles.smallImg} source={getImgByShape(figure.shapes[1])} />
                                        </View>
                                    </View>
                                )}
                                {level.isHideFigure(x_index, y_index) && (
                                    <View style={styles.shapeContainer}>
                                        <AntDesign style={styles.shape} name="question" size={50} color="white" />
                                    </View>
                                )}
                            </View>
                        ))}
                    </View>
                ))}
            </View>

            <View style={styles.inputsContainer}>
                <View style={styles.PressableRowFigureContainer}>
                    <Pressable onPress={() => changeUserInput(GeometryFinderShape.CIRCLE)} style={{ ...shapePressableContainer, backgroundColor: (bigSelected == GeometryFinderShape.CIRCLE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.bigImgPressable} source={circle} />
                    </Pressable>
                    <Pressable onPress={() => changeUserInput(GeometryFinderShape.SQUARE)} style={{ ...shapePressableContainer, backgroundColor: (bigSelected == GeometryFinderShape.SQUARE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.bigImgPressable} source={square} />
                    </Pressable>
                    <Pressable onPress={() => changeUserInput(GeometryFinderShape.TRIANGLE)} style={{ ...shapePressableContainer, backgroundColor: (bigSelected == GeometryFinderShape.TRIANGLE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.bigImgPressable} source={triangle} />
                    </Pressable>
                </View>
                <View style={styles.PressableRowFigureContainer}>
                    <Pressable onPress={() => changeUserInput(undefined, GeometryFinderShape.CIRCLE)} style={{ ...shapePressableContainer, backgroundColor: (smallSelected == GeometryFinderShape.CIRCLE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.smallImgPressable} source={circle} />
                    </Pressable>
                    <Pressable onPress={() => changeUserInput(undefined, GeometryFinderShape.SQUARE)} style={{ ...shapePressableContainer, backgroundColor: (smallSelected == GeometryFinderShape.SQUARE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.smallImgPressable} source={square} />
                    </Pressable>
                    <Pressable onPress={() => changeUserInput(undefined, GeometryFinderShape.TRIANGLE)} style={{ ...shapePressableContainer, backgroundColor: (smallSelected == GeometryFinderShape.TRIANGLE ? ThemeColor.BLUE : ThemeColor.PRIMARY_SHADE) }}>
                        <Image style={styles.smallImgPressable} source={triangle} />
                    </Pressable>
                </View>
            </View>
        </View>
    );
}

const shapePressableContainer = {
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 70,
    minWidth: 70,
    borderRadius: 10
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10,
    },
    figuresContainer: {
        alignSelf: 'stretch',
        flex: 5,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        //backgroundColor: "#225522"
    },
    rowFigureContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    shapeContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    shape: {
        position: 'absolute'
    },

    inputsContainer: {
        alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'center',
        flex: 3,
        paddingTop: 10,
        borderRadius: 25,
        backgroundColor: ThemeColor.PRIMARY_SHADE,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    PressableRowFigureContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexDirection: "row",
        marginVertical: 5
    },

    bigImgPressable: {
        width: 50,
        height: 50,
    },
    smallImgPressable: {
        width: 25,
        height: 25,
    },

    bigImg: {
        width: 60,
        height: 60,
    },
    smallImg: {
        width: 20,
        height: 20,
    },

    textRegular: {
        fontSize: 14,
        color: ThemeColor.SECONDARY_TEXT,
    }
})

export default GeometryFinder;