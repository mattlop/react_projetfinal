import { StyleSheet, Text, TextInput, View } from 'react-native'
import { useEffect, useRef } from 'react'
import { ThemeColor } from '../../../theme'
import React from 'react'
import { TextInputLevelData } from '../../../types'

type TextInputLevelProps = {
    level: TextInputLevelData,
    onChangeText: (text: string) => void,
    onSubmit: () => void
}

const TextInputLevel = (props: TextInputLevelProps) => {
    const { level, onChangeText, onSubmit } = props

    const textInput = useRef<TextInput>(null)

    useEffect(() => {
        textInput?.current?.focus()
        textInput?.current?.clear()
    }, [level])

    return (
        <View style={styles.container}>
            <Text style={styles.calculText}>{level.statement}</Text>
            <TextInput style={styles.input} ref={textInput}
                onChangeText={text => onChangeText(text)}
                onSubmitEditing={() => onSubmit()}
                placeholder='' keyboardType='numeric' autoFocus />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 10
    },
    calculText: {
        fontWeight: 'bold',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 40,
    },
    input: {
        height: 60,
        minWidth: 140,
        width: '80%',
        color: ThemeColor.PRIMARY_TEXT,
        fontSize: 24,
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderRadius: 10,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
        textAlign: 'center',
        //outlineColor: ThemeColor.PRIMARY_THIN,
    },
})

export default TextInputLevel