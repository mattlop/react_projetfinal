import React from 'react'
import { Pressable, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native'
import { ThemeColor } from '../../../theme'

type PickerProps<T> = {
    options: T[],
    selectedOption: T,
    renderSelectedOption(
        content: T,
    ): React.ReactElement<{}>,
    renderOption(
        content: T,
        index: number,
    ): React.ReactElement<{}>,
    onSelectedOptionChange: (option: T, index: number) => void
}

class Picker<T> extends React.Component<PickerProps<T>> {
    state = {
        isOptionsOpen: false,
        pickerContainerHeight: 0
    }

    render() {
        return (
            <View style={styles.pickerContainer}>
                <Pressable style={styles.pickerSelect} onPress={() => {
                    if (this.state.isOptionsOpen) {
                        this.setState({ ...this.state, isOptionsOpen: false })
                    } else {
                        this.setState({ ...this.state, isOptionsOpen: true })
                    }
                }} onLayout={(event) => {
                    this.setState({ ...this.state, pickerContainerHeight: event.nativeEvent.layout.height })
                }}>
                    {this.props.renderSelectedOption(this.props.selectedOption)}
                </Pressable>
                {this.state.isOptionsOpen && (
                    <SafeAreaView style={{ ...pickerOptions, top: this.state.pickerContainerHeight }}>
                        <ScrollView showsVerticalScrollIndicator={false} style={styles.pickerOptions}>
                            {this.props.options.map((option: T, index: number) => {
                                return option != this.props.selectedOption && (
                                    <Pressable style={styles.pickerOption} key={index} onPress={() => {
                                        console.log('pressed')
                                        this.props.onSelectedOptionChange(option, index)
                                        this.setState({ ...this.state, isOptionsOpen: false })
                                    }}>
                                        {this.props.renderOption(option, index)}
                                    </Pressable>
                                )
                            }
                            )}
                        </ScrollView>
                    </SafeAreaView>
                )}
            </View>
        );
    }
}

const pickerOptions = {
    maxHeight: 100,
    width: "100%",
    position: 'absolute',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
}

const styles = StyleSheet.create({
    pickerContainer: {
        flex: 1,
        alignItems: 'stretch',
    },
    pickerSelect: {
        flex: 1,
    },
    pickerOptions: {
        backgroundColor: ThemeColor.PRIMARY_THIN,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        shadowColor: ThemeColor.BLACK,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    pickerOption: {
        flex: 1,
        paddingRight: 24,
        borderTopWidth: 1,
        borderTopColor: ThemeColor.PRIMARY,
    },
})

export default Picker;