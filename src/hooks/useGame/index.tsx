import { useEffect, useState } from 'react'
import useStorage from '../useStorage'
import { GameData, gameModes, generateLevelData } from '../../../types/types'

const useGame = () => {
    const { addGameData } = useStorage()
    const [currentDate, setCurrentDate] = useState<number | undefined>()
    const [game, setGame] = useState<GameData>()
    const [isRunning, setIsRunning] = useState<boolean>(false)
    const [isResultsDisplayed, setIsResultsDisplayed] = useState<boolean>(false)
    const [currentLevelBeginTimestamp, setCurrentLevelBeginTimestamp] = useState<number>(Date.now())

    useEffect(() => {
        initDate()
        setIsRunning(false)
        setIsResultsDisplayed(false)
    }, [])

    const initDate = () => {
        const now: Date = new Date(Date.now())
        const newDate: Date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes())
        setCurrentDate(newDate.getTime())
    }

    const startGame = (gameModeIndex: number = 0, tag: string = "", training = false) => {
        if (currentDate) {
            setIsRunning(true)
            setCurrentLevelBeginTimestamp(Date.now())
            const levels = [generateLevelData(currentDate, gameModes[gameModeIndex], tag)]
            setGame({ mode: gameModes[gameModeIndex], tag: tag, name: new Date(currentDate).toLocaleString(), levels, score: 0, origin_hash: levels[0].hash.getString(), training: training })
        }
    }

    const stopGame = () => {
        if (game) {
            setIsRunning(false)
            game.levels[game.levels.length - 1].responseTime = Date.now() - currentLevelBeginTimestamp
            saveGame()
            addGameData(game)
            setIsResultsDisplayed(true)
        }
    }

    const saveGame = () => {
        if (game) {
            game.levels.forEach(level => {
                level.save()
                game.score! += level.displayableResult?.points
            })
        }
    }

    const startNextLevel = () => {
        if (game) {
            let levels = game.levels
            levels[levels.length - 1].responseTime = Date.now() - currentLevelBeginTimestamp
            if (isRunning) {
                setCurrentLevelBeginTimestamp(Date.now())
                levels = levels.concat(generateLevelData(levels[levels.length - 1].hash.getNumber(), game.mode))
            }
            setGame({ ...game, levels })
        }
    }

    const setUserInput = (text: string) => {
        if (game) {
            game.levels[game.levels.length - 1]?.setUserInput(text)
            setGame({ ...game })
        }
    }

    return {
        currentDate,
        game,
        isRunning,
        isResultsDisplayed,
        stopGame,
        setUserInput,
        startGame,
        startNextLevel
    }
}

export default useGame