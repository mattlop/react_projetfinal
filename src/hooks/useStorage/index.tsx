import * as React from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { GameData } from '../../../types/types'
import { useEffect, useState } from 'react'

const keyStorage = 'dataGame'

const useStorage = () => {

    const [gameDatas, setGameDatas] = useState<GameData[]>([])

    const [firstLoad, setFirstLoad] = useState<boolean>(false) // Sert à déterminer le premier passage dans le useEffect de gameDatas
    const [gameDatasLoaded, setGameDatasLoaded] = useState<boolean>(false) // Détermine si gameDatas est chargé

    const [dataToAdd, setDataToAdd] = useState<GameData | null>() // Variable servant de stockage temporaire
    const [dataToRemove, setDataToRemove] = useState<GameData | null>() // Variable servant de stockage temporaire

    useEffect(() => {
        getData().then((gamedatas: GameData[]) => {
            setGameDatas(gamedatas) // Préchargement des données 
        })
    }, [])

    useEffect(() => {
        if (firstLoad) {
            if (!gameDatasLoaded) {
                setGameDatasLoaded(true) // Game Data est chargé 
                if (dataToAdd != null) { // S'il y a eu une demande d'ajout avant le chargement complet de game data
                    storeGameDatas(dataToAdd)
                    setDataToAdd(null)
                }
                if (dataToRemove != null) { // S'il y a eu une demande de suppression avant le chargement complet de game data
                    eraseGameDatas(dataToRemove)
                    setDataToRemove(null)
                }
            }
        } else {
            setFirstLoad(true) // Premier passage au lancement
        }
    }, [gameDatas])

    const addGameData = (data: GameData) => {
        if (gameDatasLoaded) {
            storeGameDatas(data)
        } else {
            // gameDatas n'est pas encore chargé, ajout dans une variable temporaire
            setDataToAdd(data)
        }
    }

    const storeGameDatas = (data: GameData) => {
        const newGameDatas: GameData[] = gameDatas.concat(data)
        setGameDatas(newGameDatas)
        storeData(newGameDatas)
    }

    const removeGameData = (data: GameData) => {
        if (gameDatasLoaded) {
            eraseGameDatas(data)
        } else {
            // gameDatas n'est pas encore chargé, ajout dans une variable temporaire
            setDataToRemove(data)
        }
    }

    const eraseGameDatas = (data: GameData) => {
        const i = gameDatas.indexOf(data)
        if (i != -1) {
            const newGameDatas: GameData[] = gameDatas.slice(0, i).concat(gameDatas.slice(i + 1))
            setGameDatas(newGameDatas)
            storeData(newGameDatas)
        }
    }

    const storeData = async (data: GameData[]) => {
        try {
            const jsonValue = JSON.stringify(data)
            await AsyncStorage.setItem(keyStorage, jsonValue)
        } catch (e) {
            console.log(e)
        }
    }

    const getData = async (): Promise<GameData[]> => {
        try {
            const value = await AsyncStorage.getItem(keyStorage)
            if (value !== null) {
                return JSON.parse(value) as GameData[]
            } else {
                return []
            }
        } catch (e) {
            return []
        }
    }

    return { gameDatas, gameDatasLoaded, addGameData, removeGameData }
}

export default useStorage