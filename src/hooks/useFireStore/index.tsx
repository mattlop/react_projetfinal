import * as React from 'react'
import firebase from 'firebase/app';
import { initializeApp } from "firebase/app";
import { doc, setDoc, updateDoc, Firestore, runTransaction, arrayUnion, getDocs, query, DocumentData, collection, addDoc, getFirestore, getDoc, where, arrayRemove } from "firebase/firestore";
import useFireBaseAuth from '../useFireBaseAuth';
import { Scoreboard } from '../../../types/types';
import { useEffect, useState } from 'react';

const firebaseConfig = {
    apiKey: "AIzaSyC-_UHtxWLNsapGxFGfkgGjmig2_1XNHok",
    authDomain: "reactnativefinalproject-6f28c.firebaseapp.com",
    projectId: "reactnativefinalproject-6f28c",
    storageBucket: "reactnativefinalproject-6f28c.appspot.com",
    messagingSenderId: "703919264927",
    appId: "1:703919264927:web:51bd06fff4df44b263beaa",
    measurementId: "G-1FRSCFYQRC"
};

const SCOREBOARD_KEY = "scoreboard"
const USERS_KEY = "users"
const USERS_DOC_KEY = "info"

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const useFireStore = () => {
    const { user, isConnected } = useFireBaseAuth()
    const [ready, setReady] = useState<boolean | null>(null)

    const db = getFirestore();

    useEffect(() => {
        if (isConnected != null) {
            setReady(isConnected)
        }
    }, [isConnected])

    const setOwnScoreInScoreboard = async (score: number, tag: string): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            if (isConnected == true) {
                getDoc(doc(db, SCOREBOARD_KEY, tag)).then((scoreboard) => { // Essaie de récupéré le scoreboard
                    if (!scoreboard.exists()) { // Le scoreboard avec ce hash n'existe pas
                        setDoc(doc(db, SCOREBOARD_KEY, tag), { // Création du scoreboard
                            players: [
                                {
                                    userid: user!.uid,
                                    score: score
                                }
                            ]
                        }).then(() => {
                            resolve()
                        })
                            .catch(() => {
                                reject("error trying setting doc")
                            })
                    } else {
                        // Vérifie si le joueur est déjà dans le classement
                        const arrayPlayers: Array<any> = scoreboard.get("players")
                        const player = arrayPlayers.find((player) => {
                            if (player.userid == user!.uid) {
                                return player
                            }
                        })
                        if (player == null) { // Le joueur n'est pas dans le classement
                            updateDoc(doc(db, SCOREBOARD_KEY, tag), {
                                players: arrayUnion({ userid: user!.uid, score: score }) // On ajoute le joueur
                            }).then(() => resolve())
                                .catch(() => reject("error trying add element in array"))

                        } else { // Le joueur est déjà dans le classement
                            if (player.score >= score) {
                                reject("user already added")
                            } else {
                                updateDoc(doc(db, SCOREBOARD_KEY, tag), {
                                    players: arrayRemove(player) // On ajoute le joueur
                                }).then(() => {
                                    updateDoc(doc(db, SCOREBOARD_KEY, tag), {
                                        players: arrayUnion({ userid: user!.uid, score: score }) // On ajoute le joueur
                                    }).then(() => resolve())
                                        .catch(() => reject("error trying add element in array"))
                                })
                                    .catch(() => reject("error trying remove element in array"))
                            }
                        }
                    }
                }).catch(() => reject("error trying getting doc"))

            } else {
                reject("user not connected")
            }
        })
    }

    const getScoreboard = async (tag: string, withUsername: boolean = true): Promise<Scoreboard> => {
        return new Promise<Scoreboard>((resolve, reject) => {
            if (isConnected) {
                getDoc(doc(db, SCOREBOARD_KEY, tag)).then((scoreboard) => {
                    if (scoreboard.exists()) {
                        const sb: Scoreboard = { tag: tag, players: scoreboard.get("players") }
                        sb.players.sort((a, b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0))
                        if (withUsername) {
                            const userid: string[] = [];
                            sb.players.forEach((player) => userid.push(player.userid))
                            getUsernames(userid).then((arrayUsernames) => {
                                arrayUsernames.forEach((userinfo) => {
                                    sb.players.find((player) => player.userid == userinfo.userid)!.username = userinfo.username
                                })
                                resolve(sb)
                            }).catch((reason) => reject(reason))
                        } else {
                            resolve(sb)
                        }
                    } else {
                        reject("scoreboard not exist")
                    }
                }).catch((reason) => {
                    reject(reason)
                })
            } else {
                reject("user not connected")
            }
        })
    }

    const setOwnUserName = async (username: string): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            if (isConnected == true) {
                getDoc(doc(db, USERS_KEY, USERS_DOC_KEY)).then((usersinfo) => {
                    if (usersinfo.exists()) {
                        const arrayplayers: Array<any> = usersinfo.get("arrayusers")
                        const findplayer = arrayplayers.find((player) => {
                            if (player.userid == user!.uid) {
                                return player;
                            }
                        })
                        if (findplayer != null) {
                            updateDoc(doc(db, USERS_KEY, USERS_DOC_KEY), {
                                arrayusers: arrayRemove(findplayer)
                            }).then(() => {
                                updateDoc(doc(db, USERS_KEY, USERS_DOC_KEY), {
                                    arrayusers: arrayUnion(
                                        {
                                            userid: user!.uid,
                                            username: username
                                        }
                                    )
                                }).then(() => resolve())
                                    .catch(() => { reject("error during update") })
                            }).catch(() => { reject("error during update") })
                        } else {
                            updateDoc(doc(db, USERS_KEY, USERS_DOC_KEY), {
                                arrayusers: arrayUnion(
                                    {
                                        userid: user!.uid,
                                        username: username
                                    }
                                )
                            }).then(() => resolve())
                                .catch(() => { reject("error during update") })
                        }
                    } else {
                        reject(USERS_KEY + '/' + USERS_DOC_KEY + " path not exist")
                    }
                }).catch(() => reject("error getting : " + USERS_KEY + '/' + USERS_DOC_KEY + " path"))
            } else {
                reject("user not connected");
            }
        })
    }

    const getUsername = async (userUid: string): Promise<string> => {
        return new Promise<string>((resolve, reject) => {
            if (isConnected) {
                if (user?.displayName) resolve(user?.displayName)
                getDoc(doc(db, USERS_KEY, USERS_DOC_KEY)).then(
                    (usersinfo) => {
                        if (usersinfo.exists()) {
                            const arrayplayer: Array<any> = usersinfo.get('arrayusers')
                            const playerfinded = arrayplayer.find((player) => {
                                if (player.userid == userUid) {
                                    return player;
                                }
                            })

                            if (playerfinded != null) {
                                resolve(playerfinded.username)
                            } else {
                                reject("player not exist")
                            }
                        } else {
                            reject(USERS_KEY + '/' + USERS_DOC_KEY + " not exist")
                        }
                    }
                ).catch(() => { reject("error getting doc") })
            } else {
                reject("user not connected");
            }
        })
    }

    const getUsernames = async (usersUid: string[]): Promise<Array<{ userid: string, username?: string }>> => {
        return new Promise<Array<{ userid: string, username?: string }>>((resolve, reject) => {
            if (isConnected) {
                getDoc(doc(db, USERS_KEY, USERS_DOC_KEY)).then(
                    (usersinfo) => {
                        if (usersinfo.exists()) {
                            const arrayplayer: Array<any> = usersinfo.get('arrayusers')
                            const playersFound: Array<{ userid: string, username?: string }> = []
                            arrayplayer.forEach((player) => {
                                if (usersUid.length == 0) {
                                    return
                                }

                                const i = usersUid.findIndex((id) => { if (id == player.userid) return true })
                                if (i != -1) {
                                    playersFound.push({ userid: player.userid, username: player.username })
                                    usersUid.splice(i, 1)
                                }
                            })

                            if (usersUid.length > 0) {
                                usersUid.forEach((id) => {
                                    playersFound.push({ userid: id })
                                })
                            }

                            resolve(playersFound);
                        } else {
                            reject(USERS_KEY + '/' + USERS_DOC_KEY + " not exist")
                        }
                    }
                ).catch((error) => { reject(error) })
            } else {
                reject("user not connected");
            }
        })
    }

    return {
        setOwnUserName,
        getUsername,
        getUsernames,
        setOwnScoreInScoreboard,
        getScoreboard,
        ready,
        user
    }
}
export default useFireStore