import * as React from 'react'
import { getAuth, onAuthStateChanged, signInWithPopup, signInWithCredential, signInWithRedirect, GoogleAuthProvider, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, User, getRedirectResult } from "firebase/auth";
import { useState, useEffect } from 'react';
import { ERROR_CONNEXION } from '../../../types/types';
import { Platform } from 'react-native';
//import { GoogleSignin } from '@react-native-google-signin/google-signin';

const useFireBaseAuth = () => {
    const auth = getAuth();
    const provider = new GoogleAuthProvider();
    const [user, setUser] = useState<User | null>(null);
    const [isConnected, setIsConnected] = useState<boolean | null>(null);
    const [firstload, setFirstload] = useState<boolean>(false);

    onAuthStateChanged(auth, (user) => {
        if (user != null) {
            setUser(user)
        } else {
            //setUser(null)
            setIsConnected(false)
        }
    });

    useEffect(() => {
        /*getRedirectResult(auth).then((result) => {
            console.log("getRedirectResult result : ")
            console.log(result)
            if(result){
                setUser(result.user);
            }
        }).catch((error) => {
            console.log("getRedirectResult error : ")
            console.log(error)
        });*/
    }, [])

    useEffect(() => {
        if (!firstload) {
            setFirstload(true)
            return
        }
        if (user != null) {
            setIsConnected(true)
        } else {
            setIsConnected(false)
        }
    }, [user])

    const signIn = async (email: string, passwd: string): Promise<ERROR_CONNEXION | null> => {
        return new Promise<ERROR_CONNEXION | null>((resolve, reject) => {
            signInWithEmailAndPassword(auth, email, passwd).then((credential) => {
                setUser(credential.user)
                resolve(null)
            }).catch((reason) => {
                console.log(reason.message)
                if (reason.message == "Firebase: Error (auth/wrong-password).") {
                    reject(ERROR_CONNEXION.BAD_CREDENTIAL)
                } else if (reason.message == "Firebase: Error (auth/user-not-found).") {
                    reject(ERROR_CONNEXION.MAIL_NOT_EXIST)
                } else if (reason.message == "Firebase: Error (auth/invalid-email).") {
                    reject(ERROR_CONNEXION.MAIL_NOT_VALIDE)
                } else {
                    reject(ERROR_CONNEXION.UNKNOWN)
                }
            })
        })
    }

    const register = async (email: string, passwd: string): Promise<ERROR_CONNEXION | null> => {
        return new Promise<ERROR_CONNEXION | null>((resolve, reject) => {
            createUserWithEmailAndPassword(auth, email, passwd).then((credential) => {
                setUser(credential.user)
                resolve(null)
            }).catch((reason) => {
                console.log("register " + reason)
                if (reason.message == "Firebase: Error (auth/email-already-in-use).") {
                    reject(ERROR_CONNEXION.ALREADY_EXIST)
                } else {
                    reject(ERROR_CONNEXION.UNKNOWN)
                }
            })
        })
    }

    const signWithGoogle = async (): Promise<ERROR_CONNEXION | null> => {
        return new Promise<ERROR_CONNEXION | null>(() => {
            if (Platform.OS == "android") {
                /*GoogleSignin.signIn().then((user) => {
                    const googleCredential = GoogleAuthProvider.credential(user.idToken);
                    signInWithCredential(auth , googleCredential).then((userCred) => {
                        setUser(userCred.user)
                        console.log("sign with google")
                        return null
                    }).catch((error) => {
                        console.log("error on signin with credential")
                        console.log(error)
                    })
                    
                }).catch((error) => {
                    console.log("error on google signin")
                    console.log(error)
                    return ERROR_CONNEXION.UNKNOWN
                })*/
                return null
            }
            else if (Platform.OS == "web") {
                signInWithRedirect(auth, provider).then(() => {
                    return null;
                }).catch((error) => {
                    return ERROR_CONNEXION.UNKNOWN
                })
            }
        });
    }

    const signOutUser = () => {
        if (user != null) {
            signOut(auth)
        }
    }

    return { user, isConnected, signOutUser, signIn, register, signWithGoogle }
}

export default useFireBaseAuth;