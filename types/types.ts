import { BinToDecLevelData, CalculationLevelData, Hash, LevelData } from "."
import i18n, { Lang } from "../locale"
import GeometryFinderLevelData from "./GeometryFinderLevelData"
import HexToDecLevelData from "./HexToDecLevelData"
import { getLevelType, LevelType } from "./LevelType"

export enum GameModeType {
    NORMAL,
    CALCUL,
    BASE_CONVERSION,
    GEOMETRY_FINDER,
}

export type Scoreboard = {
    players: SBPlayer[],
    tag: string
}

export type SBPlayer = {
    userid: string,
    score: number,
    username?: string
}

export type GameMode = {
    type: GameModeType,
    name: string,
    description: string
}

export const gameModes: GameMode[] = [
    {
        type: GameModeType.NORMAL,
        name: i18n.t(Lang.NORMAL),
        description: i18n.t(Lang.NORMAL_DESCRIPTION)
    },
    {
        type: GameModeType.CALCUL,
        name: i18n.t(Lang.CALCUL),
        description: i18n.t(Lang.CALCUL_DESCRIPTION)
    },
    {
        type: GameModeType.BASE_CONVERSION,
        name: i18n.t(Lang.BASE_CONVERSION),
        description: i18n.t(Lang.BASE_CONVERSION_DESCRIPTION)
    },
    {
        type: GameModeType.GEOMETRY_FINDER,
        name: i18n.t(Lang.GEOMETRY_FINDER),
        description: i18n.t(Lang.GEOMETRY_FINDER_DESCRIPTION)
    },
]

export type GameData = {
    mode: GameMode,
    tag: string,
    name: string,
    levels: LevelData[],
    score: number,
    origin_hash: string,
    training: boolean
}

export const generateLevelData = (key: number, gameMode: GameMode = gameModes[0], tag: string = ""): LevelData => {
    const hash = new Hash(key)
    let levelType: LevelType
    switch (gameMode.type) {
        case GameModeType.NORMAL:
            levelType = getLevelType(hash.getNumber())
            break
        case GameModeType.CALCUL:
            levelType = LevelType.CALCUL
            break
        case GameModeType.BASE_CONVERSION:
            const possibleLevels = [LevelType.BIN_TO_DEC, LevelType.HEX_TO_DEC]
            levelType = possibleLevels[hash.getNumber() % possibleLevels.length]
            break
        case GameModeType.GEOMETRY_FINDER:
            levelType = LevelType.GEOMETRY_FINDER
            break
        default:
            levelType = getLevelType(hash.getNumber())
            break
    }

    switch (levelType) {
        case LevelType.CALCUL:
            return new CalculationLevelData(hash)
        case LevelType.BIN_TO_DEC:
            return new BinToDecLevelData(hash)
        case LevelType.HEX_TO_DEC:
            return new HexToDecLevelData(hash)
        case LevelType.GEOMETRY_FINDER:
            return new GeometryFinderLevelData(hash)
        default:
            throw new Error(`LevelType not supported: ${levelType}`)
    }
}

export enum ERROR_CONNEXION {
    MAIL_NOT_EXIST = "mail doesn't exist",
    BAD_CREDENTIAL = "bad credential",
    ALREADY_EXIST = "credential already exist",
    MAIL_NOT_VALIDE = "mail is not valid",

    UNKNOWN = "error not identified"
}
