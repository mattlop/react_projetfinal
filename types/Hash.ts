import md5 from "md5"

export default class Hash {
    private hash: number

    constructor(value: number) {
        const md5parsed = md5(value.toString()).substring(0, 10)
        this.hash = parseInt(md5parsed, 16)
    }

    public getNumber = (): number => {
        return this.hash
    }

    public getString = (): string => {
        return this.hash.toString()
    }

    public getSubString = (indexStart: number, indexEnd: number): string => {
        return this.getString().substring(indexStart, indexEnd)
    }

    public getSubNumber = (indexStart: number, indexEnd: number): number => {
        return parseInt(this.getSubString(indexStart, indexEnd))
    }
}