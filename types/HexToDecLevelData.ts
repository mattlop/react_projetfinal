import { Hash, TextInputLevelData } from "."
import { LevelType } from "./LevelType"

export default class HexToDecLevelData extends TextInputLevelData {

    public hexaNumber: string
    private result: number

    constructor(public hash: Hash) {
        super(hash)
        this.levelType = LevelType.HEX_TO_DEC

        this.hexaNumber = Number(hash.getString()).toString(16).substring(1, 3).toUpperCase()
        this.result = parseInt(this.hexaNumber, 16)
        this.statement = this.hexaNumber

        while (this.statement.length < 2) {
            this.statement = `0${this.statement}`
        }
    }

    public save = () => {
        this.displayableResult = {
            points: this.getPoints(),
            statement: this.getDisplayableStatment(),
            result: this.getDisplayableResult(),
            userInput: this.getDisplayableUserInput(),
        }
    }

    protected getPoints = (): number => {
        let successPoints = 4
        return this.result.toString() === this.userInput ? successPoints : 0
    }

    protected getDisplayableResult = (): string => {
        return `${this.result}`
    }
}