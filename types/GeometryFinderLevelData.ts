import Hash from "./Hash";
import LevelData from "./LevelData"
import { LevelType } from "./LevelType"


export type GeometryFinderFigure = {
    index_x: number,
    index_y: number,
    shapes: GeometryFinderShape[]
}

export enum GeometryFinderShape {
    SQUARE = 0,
    CIRCLE = 1,
    TRIANGLE = 2
}
export default class GeometryFinderLevelData extends LevelData {

    public tabshapes: GeometryFinderFigure[][]

    private readonly SHAPE_LENGTH: number = 3;

    private startFirstShape: GeometryFinderShape
    private startSecondShape: GeometryFinderShape

    private rotationXFirst: number
    private rotationYFirst: number

    private rotationXSecond: number
    private rotationYSecond: number

    private hideFigureIndex: number = 8

    constructor(public hash: Hash) {
        super(hash)
        this.levelType = LevelType.GEOMETRY_FINDER
        this.startFirstShape = getShape(hash.getSubNumber(0, 1))
        this.startSecondShape = getShape(hash.getSubNumber(1, 2))

        this.rotationXFirst = getRotation(hash.getSubNumber(2, 3))
        this.rotationYFirst = getRotation(hash.getSubNumber(4, 5))

        this.rotationXSecond = getRotation(hash.getSubNumber(5, 6))
        this.rotationYSecond = getRotation(hash.getSubNumber(7, 8))

        this.hideFigureIndex = hash.getSubNumber(8, 9) % 9;

        //this.checkRotation()
        this.tabshapes = this.generateFigures()
    }

    public static UserInputToShapes = (lvl: GeometryFinderLevelData): GeometryFinderShape[] => {
        const tab = lvl.userInput.split(';')
        const result: GeometryFinderShape[] = []

        for (let index = 0; index < tab.length; index++) {
            switch (Number(tab[index])) {
                case 0:
                    result.push(GeometryFinderShape.SQUARE)
                    break
                case 1:
                    result.push(GeometryFinderShape.CIRCLE)
                    break
                case 2:
                    result.push(GeometryFinderShape.TRIANGLE)
                    break
            }
        }

        return result
    }

    public static isHideFigureStatic = (x: number, y: number, lv: GeometryFinderLevelData) => {
        return (lv.hideFigureIndex == (x + (y * lv.SHAPE_LENGTH)))
    }

    public isHideFigure = (x: number, y: number): boolean => {
        return (this.hideFigureIndex == (x + (y * this.SHAPE_LENGTH)))
    }

    private checkRotation = () => {

        if (this.rotationXFirst < 0 && this.rotationXSecond > 0) {
            if (Math.abs(this.rotationXFirst) + this.rotationXSecond == 3) {
                this.rotationXFirst = Math.abs(this.rotationXFirst);
            }
        }
        if (this.rotationXFirst > 0 && this.rotationXSecond < 0) {
            if (Math.abs(this.rotationXSecond) + this.rotationXFirst == 3) {
                this.rotationXSecond = Math.abs(this.rotationXSecond);
            }
        }

        if (this.rotationYFirst < 0 && this.rotationYSecond > 0) {
            if (Math.abs(this.rotationYFirst) + this.rotationYSecond == 3) {
                this.rotationYFirst = Math.abs(this.rotationYFirst);
            }
        }
        if (this.rotationYFirst > 0 && this.rotationYSecond < 0) {
            if (Math.abs(this.rotationYSecond) + this.rotationYFirst == 3) {
                this.rotationYSecond = Math.abs(this.rotationYSecond);
            }
        }
    }

    // TEST ONLY
    public getResult() {
        return this.getDisplayableResult()
    }

    public setUserInput = (userInput: string) => {
        this.userInput = userInput
    }

    public save = () => {
        this.displayableResult = {
            points: this.getPoints(),
            statement: this.getDisplayableStatment(),
            result: this.getDisplayableResult(),
            userInput: this.getDisplayableUserInput(),
        }
    }

    protected getPoints = (): number => {
        const inputs: string[] = this.getDisplayableUserInput().split(';');
        if (inputs.length != 2) return 0;

        const results = this.getDisplayableResult().split(';');

        if (results[0] == inputs[0] && results[1] == inputs[1]) {
            return 1;
        }

        return 0;
    }

    protected getDisplayableResult = (): string => {
        const x = this.hideFigureIndex % this.SHAPE_LENGTH;
        const y = (this.hideFigureIndex - x) / this.SHAPE_LENGTH;
        return (
            (this.tabshapes[y][x].shapes[0] as number).toString()
            + ';' +
            (this.tabshapes[y][x].shapes[1] as number).toString()
        )
    }

    protected getDisplayableUserInput = (): string => {
        return this.userInput !== undefined ? this.userInput : "-1;-1";
    }

    private generateFigures = (): GeometryFinderFigure[][] => {

        const allfigures: GeometryFinderFigure[][] = [];

        for (let y = 0; y < this.SHAPE_LENGTH; y++) {
            const arrX: GeometryFinderFigure[] = [];
            for (let x = 0; x < this.SHAPE_LENGTH; x++) {
                arrX.push({
                    index_x: x,
                    index_y: y,
                    shapes: this.generateFigureShapes(x, y)
                })
            }
            allfigures.push(arrX);
        }

        return allfigures;
    }

    private generateFigureShapes = (x: number, y: number): GeometryFinderShape[] => {
        const shapes: GeometryFinderShape[] = [];

        shapes.push(
            getShape(this.startFirstShape as number +
                this.rotationXFirst * x + this.rotationYFirst + y
            )
        )
        shapes.push(
            getShape(this.startSecondShape as number +
                this.rotationXSecond * x + this.rotationYSecond * y
            )
        )


        return shapes;
    }
}

const getShape = (digit: number): GeometryFinderShape => {
    while (digit < 0) digit = 3 - digit
    const shapeIndex = digit % 3

    switch (shapeIndex) {
        case 0:
            return GeometryFinderShape.CIRCLE
        case 1:
            return GeometryFinderShape.TRIANGLE
        case 2:
            return GeometryFinderShape.SQUARE
        default:
            console.log("WARNING : " + shapeIndex)
            return GeometryFinderShape.CIRCLE
    }
}

const getRotation = (digit: number): GeometryFinderShape => {
    while (digit < 0) digit = 4 - digit
    const shapeIndex = digit % 4

    switch (shapeIndex) {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return -1
        case 2:
            return -2

        default:
            return 1
    }
}
