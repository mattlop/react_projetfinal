
export enum LevelType {
    CALCUL = "Calcul",
    BIN_TO_DEC = "Bin to Dec",
    HEX_TO_DEC = "Hex to Dec",
    GEOMETRY_FINDER = "Geometry finder"
}

export const getLevelType = (digit: number): LevelType => {
    let levelTypeIndex = digit % Object.keys(LevelType).length
    if (levelTypeIndex < 0 || levelTypeIndex >= Object.keys(LevelType).length) {
        console.log(`getLevelType error, digit: ${digit}, levelType: ${levelTypeIndex}, Object.keys(LevelType).length: ${Object.keys(LevelType).length}`)
    }
    switch (levelTypeIndex) {
        case 0:
            return LevelType.CALCUL
        case 1:
            return LevelType.BIN_TO_DEC
        case 2:
            return LevelType.HEX_TO_DEC
        case 3:
            return LevelType.GEOMETRY_FINDER
        default:
            return LevelType.CALCUL
    }
}
