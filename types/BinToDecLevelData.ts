import { Hash, TextInputLevelData } from "../types"
import { LevelType } from "./LevelType"

export default class BinToDecLevelData extends TextInputLevelData {

    public binaryNumber: number
    private result: number

    constructor(public hash: Hash) {
        super(hash)
        this.levelType = LevelType.BIN_TO_DEC

        this.binaryNumber = +Number(hash.getNumber()).toString(2).substring(1, 9)
        this.result = parseInt(`${this.binaryNumber}`, 2)
        this.statement = this.binaryNumber.toString()

        while (this.statement.length < 8) {
            this.statement = `0${this.statement}`
        }
    }

    public save = () => {
        this.displayableResult = {
            points: this.getPoints(),
            statement: this.getDisplayableStatment(),
            result: this.getDisplayableResult(),
            userInput: this.getDisplayableUserInput(),
        }
    }

    protected getPoints = (): number => {
        let successPoints = 4
        return this.result.toString() === this.userInput ? successPoints : 0
    }

    protected getDisplayableResult = (): string => {
        return `${this.result}`
    }
}