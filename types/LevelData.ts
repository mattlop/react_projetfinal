import { Hash } from "."
import { LevelType } from "./LevelType"

export default abstract class LevelData {
    constructor(public hash: Hash) { }

    protected userInput = ''
    public levelType?: LevelType
    /** In milliseconds */
    public responseTime: number = 0
    public setUserInput = (userInput: string): void => { console.log("LevelData.setUserInput") }
    public displayableResult: {
        points: number,
        statement: string,
        result: string,
        userInput: string
    } = {
            points: 0,
            statement: "",
            result: "",
            userInput: "",
        }
    protected getPoints = (): number => { console.log("LevelData.getPoints"); return 0 }
    protected getDisplayableStatment = (): string => { console.log("LevelData.getDisplayableStatment"); return "" }
    protected getDisplayableResult = (): string => { console.log("LevelData.getDisplayableResult"); return "" }
    protected getDisplayableUserInput = (): string => { console.log("LevelData.getDisplayableUserInput"); return "" }

    public save = (): void => { console.log("LevelData.save") }
}