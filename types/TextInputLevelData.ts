import { Hash, LevelData } from "../types"

export default abstract class TextInputLevelData extends LevelData {

    public statement?: string

    constructor(public hash: Hash) {
        super(hash)
    }

    public setUserInput = (userInput: string) => {
        this.userInput = userInput
    }

    public save = (): void => { console.log("TextInputLevelData.save") }

    protected getPoints = (): number => { console.log("TextInputLevelData.getPoints"); return 0 }
    protected getDisplayableStatment = (): string => { return this.statement || "" }
    protected getDisplayableResult = (): string => { console.log("TextInputLevelData.getDisplayableResult"); return "" }
    protected getDisplayableUserInput = (): string => {
        if (this.userInput !== undefined && `${this.userInput}`.length > 0) {
            return `${this.userInput}`
        }
        return '...'
    }
}
