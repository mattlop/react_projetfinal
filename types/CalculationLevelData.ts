import { Hash, TextInputLevelData } from "../types"
import { LevelType } from "./LevelType"

export default class CalculationLevelData extends TextInputLevelData {

    public symbol: CalculationSymbol
    public leftNumber: number
    public rightNumber: number
    private result?: number

    constructor(public hash: Hash) {
        super(hash)
        this.levelType = LevelType.CALCUL
        this.symbol = getSymbol(hash.getSubNumber(0, 1))

        let number1 = hash.getSubNumber(1, 3)
        let number2 = hash.getSubNumber(3, 5)

        if (this.symbol === CalculationSymbol.MULTIPLY || this.symbol === CalculationSymbol.DIVIDE) {
            number1 = hash.getSubNumber(1, 2)
            number2 = hash.getSubNumber(2, 3)
        }

        this.leftNumber = number1
        this.rightNumber = number2

        if (this.symbol === CalculationSymbol.DIVIDE) {
            if (number1 < number2) {
                this.leftNumber = number2
                this.rightNumber = number1
            }
            if (this.rightNumber === 0) {
                this.rightNumber += 1
            }
        }

        this.statement = `${this.leftNumber} ${this.symbol} ${this.rightNumber}`
    }

    public setUserInput = (userInput: string) => {
        this.userInput = userInput
    }

    public save = () => {
        this.displayableResult = {
            points: this.getPoints(),
            statement: this.getDisplayableStatment(),
            result: this.getDisplayableResult(),
            userInput: this.getDisplayableUserInput(),
        }
    }

    protected getPoints = (): number => {

        let successPoints = 1
        switch (this.symbol) {
            case CalculationSymbol.PLUS:
                this.result = this.leftNumber + this.rightNumber
                successPoints = 1
                break
            case CalculationSymbol.MINUS:
                this.result = this.leftNumber - this.rightNumber
                successPoints = 1
                break
            case CalculationSymbol.MULTIPLY:
                this.result = this.leftNumber * this.rightNumber
                successPoints = 2
                break
            case CalculationSymbol.DIVIDE:
                this.result = Math.floor(this.leftNumber / this.rightNumber)
                successPoints = 2
                break
        }
        return this.result.toString() === this.userInput ? successPoints : 0
    }

    protected getDisplayableResult = (): string => {
        return `${this.result}`
    }

    protected getDisplayableUserInput = (): string => {
        if (this.userInput !== undefined && `${this.userInput}`.length > 0) {
            return `${this.userInput}`
        }
        return '...'
    }

}

enum CalculationSymbol {
    PLUS = "+",
    MINUS = "-",
    MULTIPLY = "x",
    DIVIDE = "÷"
}

const getSymbol = (digit: number): CalculationSymbol => {
    const symbolIndex = digit % Object.keys(CalculationSymbol).length
    switch (symbolIndex) {
        case 0:
            return CalculationSymbol.PLUS
        case 1:
            return CalculationSymbol.MINUS
        case 2:
            return CalculationSymbol.MULTIPLY
        case 3:
            return CalculationSymbol.DIVIDE
        default:
            return CalculationSymbol.PLUS
    }
}