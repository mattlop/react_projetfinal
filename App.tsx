import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import * as NavigationBar from 'expo-navigation-bar'
import { StyleSheet, Platform, View, StatusBar, Linking } from 'react-native'
import { Game, HistoryPage, Home, Profile, Ranking, Rules } from './src/pages'
import AppLoading from 'expo-app-loading'
import { useEffect } from 'react'
//import firebase from 'react-native-firebase'
import { App as AppCapacitor, URLOpenListenerEvent } from '@capacitor/app'
import {
  useFonts,
  RobotoSlab_100Thin,
  RobotoSlab_200ExtraLight,
  RobotoSlab_300Light,
  RobotoSlab_400Regular,
  RobotoSlab_500Medium,
  RobotoSlab_600SemiBold,
  RobotoSlab_700Bold,
  RobotoSlab_800ExtraBold,
  RobotoSlab_900Black
} from '@expo-google-fonts/roboto-slab'
import { ThemeColor } from './theme'
import i18n, { Lang } from './locale'

const Stack = createNativeStackNavigator()

export default function App() {

  const [loaded] = useFonts({
    // RobotoSlab_100Thin,
    // RobotoSlab_200ExtraLight,
    // RobotoSlab_300Light,
    // RobotoSlab_400Regular,
    // RobotoSlab_500Medium,
    // RobotoSlab_600SemiBold,
    // RobotoSlab_700Bold,
    // RobotoSlab_800ExtraBold,
    // RobotoSlab_900Black
  })

  useEffect(() => {
    Linking.getInitialURL()
      .then((url) => {
        // console.log("deep link")
        // console.log(url)
      })
      .catch(err => err);
  }, [])

  useEffect(() => {
    if (Platform.OS === "android" && loaded) {
      StatusBar.setBackgroundColor(ThemeColor.PRIMARY_SHADE)
      StatusBar.setBarStyle('light-content')
      NavigationBar.setBackgroundColorAsync(ThemeColor.PRIMARY)
      AppCapacitor.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
        const slug = event.url.split('.app').pop();
        if (slug) {
          console.log(slug)
        }
      });
    }
  }, [loaded])

  if (!loaded) {
    return (<AppLoading />)
  }

  return (
    <View style={{ flex: 1, backgroundColor: ThemeColor.PRIMARY, marginTop: StatusBar.currentHeight }}>

      <NavigationContainer theme={{
        dark: true,
        colors: {
          primary: ThemeColor.PRIMARY,
          background: ThemeColor.PRIMARY,
          card: ThemeColor.PRIMARY,
          text: ThemeColor.PRIMARY_TEXT,
          border: ThemeColor.PRIMARY,
          notification: ThemeColor.PRIMARY,
        }
      }
      }>

        {loaded && (
          <Stack.Navigator
            screenOptions={{}}>
            <Stack.Screen
              options={{
                headerShown: false
              }}
              name="Home"
              component={Home}
            />
            <Stack.Screen
              name="Game"
              component={Game}
              options={{
                title: i18n.t(Lang.PLAY),
                headerStyle: {
                  backgroundColor: ThemeColor.PRIMARY_SHADE,
                },
                headerShadowVisible: false,
                headerTintColor: ThemeColor.PRIMARY_TEXT,
                headerTitleStyle: {
                  //fontFamily: 'RobotoSlab_700Bold'
                },
              }}
            />
            <Stack.Screen
              name="Rules"
              component={Rules}
              options={{
                title: i18n.t(Lang.RULES),
                headerStyle: {
                  backgroundColor: ThemeColor.PRIMARY_SHADE,
                },
                headerShadowVisible: false,
                headerTintColor: ThemeColor.PRIMARY_TEXT,
                headerTitleStyle: {
                  //fontFamily: 'RobotoSlab_700Bold'
                },
              }}
            />
            <Stack.Screen
              name="History"
              component={HistoryPage}
              options={{
                title: i18n.t(Lang.HISTORY),
                headerStyle: {
                  backgroundColor: ThemeColor.PRIMARY_SHADE,
                },
                headerShadowVisible: false,
                headerTintColor: ThemeColor.PRIMARY_TEXT,
                headerTitleStyle: {
                  //fontFamily: 'RobotoSlab_700Bold'
                },
              }}
            />
            <Stack.Screen
              name="Profile"
              //component={(props) => <Profile {...props} isConnected={isConnected} />}
              component={Profile}
              options={{
                title: i18n.t(Lang.PLAYER),
                headerStyle: {
                  backgroundColor: ThemeColor.PRIMARY_SHADE,
                },
                headerShadowVisible: false,
                headerTintColor: ThemeColor.PRIMARY_TEXT,
                headerTitleStyle: {
                  //fontFamily: 'RobotoSlab_700Bold'
                },
              }}
            />
            <Stack.Screen
              name="Ranking"
              //component={(props) => <Profile {...props} isConnected={isConnected} />}
              component={Ranking}
              options={{
                title: i18n.t(Lang.RANKING),
                headerStyle: {
                  backgroundColor: ThemeColor.PRIMARY_SHADE,
                },
                headerShadowVisible: false,
                headerTintColor: ThemeColor.PRIMARY_TEXT,
                headerTitleStyle: {
                  //fontFamily: 'RobotoSlab_700Bold'
                },
              }}
            />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    </View>
  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#f00',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
